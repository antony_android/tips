<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$host = gethostname();
$baseUri = '/';

$connection = array(
    'adapter' => 'Mysql',
    'host' => '127.0.0.1',
    'username' => 'anthony',
    'password' => '@smarts0k0T1p254',
    'dbname' => 'tips',
    'charset' => 'utf8'
);

return new \Phalcon\Config(array(
    'database' => $connection,
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'layoutsDir'     => APP_PATH . '/app/views/layouts',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'vendorDir'      => APP_PATH . '/vendor/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => $baseUri,
    ),
    'redis' => [
		'prefix' => 'surebet_',
		'host'   => '35.184.232.101',
		'port'   =>  6379,
		'auth'   => 'SM1BtfDiALdh', 
		'persistent' => 1,
    ]
));
