<table id="main" class="mybets">
  <tr>
  </tr>
  <td>
    <table class="highlights--item" width="100%">
      <tr>
        <td style="padding: 0;">
          <!-- {{ this.flashSession.output() }} -->

          <?php 
            $created = strtotime($myTip['created_at']);
            $nowT = date("Y-m-d H:i:s");
            $nowTime = strtotime($nowT);
            $interval  = abs($nowTime - $created);
            $minutes   = round($interval / 60);
          ?>
          <table class="login" width="100%" cellpadding="0" cellspacing="0">
            <?php 

              $diff = strtotime(date('Y-m-d H:i:s')) - strtotime($myTip['created_at']);
              $pasttime = $diff/60/60;
      
    <tr>
      <td>
      <table class="table basic-table table-responsive bd" width="94%" 
      style="text-align:left; border:none; margin-left:12px; margin-top:12px; margin-right:12px; 
      border-collapse:collapse;"> 
        <thead class="" style="background:#999; color:#fff"> 
          <tr> <th>Tip Number</th> <th>Date</th> <th>Game ID</th> </tr> 
        </thead> 
        <tbody>
          <tr> 
          <td><a href="#"><?= $myTip['user_tip_id'] ?></a>
          </td>
          <td>
            <?= date("d/m H:i", strtotime($myTip["created_at"])); ?>
          </td>
          <td>
            <?= $myTip["game_id"] ?>
          </td>
        </tr>
        <tr>
          <td colspan="4">
            <h3 class="events">Tips (
              <?= sizeof($tipDetails) ?>)
            </h3>
          </td>
        </tr>
        <tr>
          <td colspan="6">
            <table class="table bdd" width="100%" style="border-collapse:collapse">
              <thead class="table-h">
                <tr>
                  <th>Game</th>
                  <th>Odds</th>
                  <th>Tip</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($tipDetails as $tip): ?>
                <tr style="border-top:1px solid #eee;">
                  <td>
                    <?= $tip['home_team']."<span classs='mobi visible-mobile'> <br/>
                     </span> <span class='web-element'>V</span>".$tip['away_team'] ?>
                  </td>
                  <td>
                    <?= $tip['odd_value'] ?>
                  </td>
                  <td>
                    <?= $tip['prediction'] ?>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </td> <!-- colspan6 -->
        </tr>
        </tbody>
      </table>

    </td>
    </tr>
  </table>