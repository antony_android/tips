
<table id="main">
    <tr>
      <td>
        <table class="mybets">
            <th class="title">How to Get Tips</th>
            <table class="terms">
            <tr>
              <td style="line-height: 1.5;">
                <ol>
                    <li>Click on <strong>"Add" </strong>to add the tip for the match</li>
                    <li>Continue adding tips for the games that you want. Your games are added to the slip </li>
                    <li>When done with adding the matches, click on <strong>"Get Tips"</strong> or click on the slip</li>
                    <li>You will be taken to the slips page. If you are not logged in input your number</li>
                    <li>Click on <strong>"Get Tips"</strong>. If you have insufficient credits, an MPESA prompt will be sent to your phone</li>
                    <li>Complete the MPESA payment</li>
                    <li>Your tips will be sent to your phone number and saved. You can view your tips by logging in and visiting <strong>"My Tips"</strong> page</li>
                </ol>
              </td>
            </tr>
            </table>
          </table>
      </td>
    </tr>
  </table>