<style>
ul{
    padding:0;
}
ul li{
   list-style:none; 
}

ul li h1{
    font-size:14px;
}
p{
   margin-left:0px !important;
   font-size:12px !important;
}
h2{
    margin:0px;
}
</style>
<div class="panel-header primary-bg">
	<h2>
		<font style="vertical-align: inherit;">
			<font style="vertical-align: inherit;">Terms and Conditions</font>
		</font>
	</h2>
</div>

<ul>
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Introduction: SureBet Terms and conditions.</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">These general Terms and Conditions apply for all betting games offered by Risecom Limited trading as SureBet by registration and being active using our SMS,USSD Codes, web applications or mobile apps .</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">With consultation with regulating body BCLB, Surebet reserve the right to add or update terms and conditions at any time. In such circumstances the posting of the new rules on the website or any other recognized means will be deemed to be the time they become effective and they will take immediate effect. Any bets placed after that time will be subject to the updated rules. By placing a bet, you agree with updated Rules.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">The user is responsible in ensuring that they don&rsquo;t infringe on any laws in their jurisdiction when opening an account and conducting business with SureBet at all times during the subsistence of their subscription/ participation.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet will accept no liability from third parties whatsoever, resulting from you providing incorrect or false data.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">One customer may open only one account. Should we identify any customer with more than one account, we reserve the right to close these accounts. If SureBet detects multiple accounts which have been setup deliberately with misleading information, have displayed criminal behavior, or if SureBet concludes that the account-owner has fraudulent intentions, SureBet shall be entitled to close the accounts and confiscate all funds, including initial deposit.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet reserves the right to undertake the following:</p>
<ul>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-top: 0.19in;">Verify your identity</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Verify your gaming activity</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Conduct security and other internal procedures in relation to your account</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Ensure that prizes offered and award procedures have been adhered to</p>
    </li>
    <li>
        <p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;">Disclose winners&apos; personal information (name, images etc) on any media platform for marketing purposes</p>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.75in;margin-top: 0.19in;"><br><br></p>
<ul start="2">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">DEFINITIONS</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">In these rules the following words have the following meanings unless the context requires otherwise</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>SureBet&nbsp;</strong>: Trading name owned by Risecom limited , a limited company registered in Kenya.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Surebet.ke</strong>: website owned by Risecom for purpose of online betting.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Bet</strong> : The staking of money on the prediction of the outcome of a sporting event offered by us. A bet can include one or more predictions. A bet on one outcome is a single. A bet on many different outcomes is a multiple.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Odds or Price</strong>: The return on the money staked should the prediction recorded in a Bet be correct.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Customer</strong>: legal person placing a bet with SureBet</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>User</strong>: legal person using this service</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Full time (FT)</strong>: The point when the official 90 minutes of a full match has elapsed plus any &apos;Injury Time&apos; that the referee chooses to add and shall NOT include scheduled extra time (two half times of 15 minutes each), penalty shoot-outs etc.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>VOID</strong> : That for one reason or another the bet is not acceptable to SureBet. In these cases, this bet is settled at the price of 1. In the case of a single bet this would mean the stake is returned to the customer.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Notwithstanding anything contained in these Terms and Conditions, the Customer&apos;s winnings shall be subject to the governing regulations in the Republic of Kenya and this shall include but not be limited to applicable taxes and levies.</p>
<ul start="3">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Customer identity.</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">It is the customer&rsquo;s responsibility to ensure that their login, password and other account details are kept private and secure at all times. Do not disclose these details to any third party. Be aware that all bets, placed by anyone quoting your secure details, will be attributed to your account. We strongly advise that you do not use the same passwords that you use for your email, social media or other online services, as this poses a risk of unauthorised access to your surebet account. Please use a unique and strong password for your account. Contact our Customer Support team immediately if you have any concerns about the security of your account.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">If we have any concerns about the identity of a customer, we reserve the right to request any ID documentation that we deem appropriate and withhold any payments and/or restrict the withdrawal functionality on the account until we are satisfied of the authenticity of all the details we have been given. Should the details we have been given prove to be false, we reserve the right to void any bets placed on this account and to withhold any payments indefinitely.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">In addition, we cannot do account transactions for accounts which are not identical to the phone number. This means that you cannot, for example, ask to be paid out on a different number other than the number you have placed a bet with; also you cannot send money to us and request that this is placed in a different account. In addition to your phone number your initial transaction will initiate the creation of a virtual account to which all monies added to the cause of betting will be held.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Note: This is not a standard banking account or an E-wallet and it shall not be used to keep money other than the use to place bets.</p>
<ul start="4">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">AGE RESTRICTION</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Kenya Law prohibits under 18 year olds from betting.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">You must be over the age of 18 and over the legal age for Internet.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">We reserve the right to verify a customer&apos;s age using recognized age verification third party companies. Further, if necessary we may also request proof of age or identification documentation from the customer at any time. During this period accounts may be suspended whilst checks to confirm verification are being completed.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">By accepting the terms and conditions of registration you are confirming and indicating to us that you are 18 years of age or above, and over the legal age for Internet Gambling in kenya.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">We reserve the right to use any public shared or any form of information available to investigate and implement age restrictions. Members of the public can share such through our social media platforms.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><br></p>
<ul start="5">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Responsible Gambling</h1>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">The most important aspect of you being part of the Surebet community is that you enjoy your time with us. This means having all the information and facilities that you need to make informed decisions about your spending</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;"><br></p>
<ul start="6">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Minors and Advice for Family &amp; Friends</h1>
    </li>
</ul>
<ul>
    <li>
        <h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'><u>Minors</u></h2>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">If you are under 18, please do not attempt to register an account with Surebet.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">As a socially responsible operator, Surebet employs an Over 18 Policy and are committed to preventing underage gambling. Verification checks are conducted on Surebet customers, so if you register a Surebet account, please do not be offended in the event that we ask you for proof of age and identity.</p>
<ul>
    <li>
        <h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Parental Guidance</h2>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">If you have concerns that your children or someone you know under the age of 18 may be gambling through this site or has access to personal and account details that would allow them to do so, then there are a number of options available to help you.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">The following tools are useful to check and block underage access to our site and sites containing age specific content:</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><u><a href="http://www.cybersitter.com/" target="_blank">www.cybersitter.com</a></u></span> &ndash; this allows specific sites to be blocked</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><u><a href="http://www.netnanny.com/" target="_blank">www.netnanny.com</a></u></span> &ndash; this has filtering software protecting children from inappropriate web content</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><u><a href="http://www.bigdeal.org.uk/" target="_blank">www.bigdeal.org.uk</a></u></span> &ndash; this service is offered by GamCare to provide young people with information and guidance around responsible gambling</p>
<ul>
    <li>
        <h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Family &amp; Friends</h2>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">We understand that an individual developing a gambling problem does not just affect them alone, but also their family and friends.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Situations like this can be very difficult to approach. The key is to offer your support throughout and outline the additional support options available when they are ready to consider them. Please see the Getting Assistance section of our website for details of leading organisations that may be able to help customers directly.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">For any people close to an existing customer whose gambling behaviour they are concerned about, we recommend that you talk to a counselor on toll free numbers on our website.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><br></p>
<ul start="7">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Staying In Control</h1>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Below is a list of self-test questions which you can use to assess whether you may be spending too much time and/or money gambling.</p>
<ul>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Have others ever criticised your gambling?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Have you ever lied to cover up the amount of money or time you have gambled?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Do arguments, frustrations or disappointments make you want to gamble?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Do you gamble alone for long periods?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Do you stay away from work to gamble?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Do you gamble to escape from a boring or unhappy life?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Are you reluctant to spend &apos;gambling money&apos; on anything else?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Have you lost interest in your family, friends or pastimes due to gambling?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">After losing, do you feel you must try and win back your losses as soon as possible?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">When gambling and you run out of money, do you feel lost and in despair, and need to gamble again as soon as possible?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Do you gamble until your last penny is gone?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Have you lied, stolen or borrowed just to get money to gamble or pay gambling debts?</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Do you feel depressed or anxious because of your gambling?</p>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">If you find you are answering &apos;yes&apos; to even a couple of the above questions, you might be developing an addiction, please talk to a counselor.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Please remember these five tips as a guide to help you gamble more responsibly:</p>
<ul>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Gamble only if you are still finding it fun</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Think of the money you lose as the cost of entertainment and accept losing as part of the game</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Don&apos;t gamble to win back losses</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Set a monetary and/or a time limit and stick to it</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Don&apos;t borrow money to gamble</p>
    </li>
</ul>
<ul start="8">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Account Closure and Self-Exclusion</h1>
    </li>
</ul>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">Account Closure</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Should you wish to close your Surebet account, please contact our Customer Support.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">On self help, please send &ldquo;stop/quit/close account&rdquo; to sms shortcode 29488.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">In very few of cases, there will be the option for you to reopen your account. However, should you request for a permanent closure, then we shall honour this request in all circumstances and reopening shall not be permitted.</p>
<ul start="9">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Self-Exclusion</h1>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">If you have concerns about your gambling activity and after considering your position, you feel that you can no longer afford to continue spending any time or money with us, there is the option to self-exclude.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Self-exclusion is a voluntary fixed term in which you are denied access to our betting services. This period can be set at a minimum of 6 months, up to 5 years. There is also the option to self-exclude permanently.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">If you choose to self-exclude, these are the key points to consider:</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">You will not be able to bet and deposit with Surebet at all during this time.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Please remember that once you have self-excluded, this agreement is final and cannot be changed. We will not be able to reactivate the bet facility on your account during this period.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">To protect and prevent you from using Surebet during this time, if we detect any accounts open in your name, such accounts will be closed. Surebet cannot be liable for any losses accumulated from customers opening any accounts when they are already self-excluded.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Upon requesting self-exclusion, if you have an outstanding balance left in your account, you will still be able to withdraw funds.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">We would kindly ask that you update your contact details (address and/or telephone number) if they change so we can put protecting measures against opening new accounts with updated contact details. In the event that inaccurate or incomplete data is submitted or your current details are not updated, we will not be held responsible for any subsequent losses or other circumstances caused by these actions.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">You can activate a self-exclusion by contacting our Customer Support. Please see the <span style="border: none;padding: 0in;"><u><a href="https://www.marathonbet.com/en/help/contact_us" target="Contact Us">Contact Us</a></u></span> section of our website for further details.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">As a socially responsible betting operator, Surebet will endeavour to action your account closure/self-exclusion request at the earliest possible opportunity. However, depending on the contact channel you use to close or self-exclude, the time period in which your request will be actioned in full may vary. Therefore, Surebet shall not be liable for any losses towards either party before the closure takes place or the self-exclusion period comes into effect. Please note that your closure/self-exclusion will only come into effect at the point you receive confirmation from our Customer Support that your account is now closed/self-excluded. If you discover the account you have requested to be closed/self-excluded is still open please call our Customer Support immediately and we will treat it with the utmost urgency.</p>
<ul start="10">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Continuation.</h1>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">The Company reserves the right to close any account immediately and void any bets placed from such an account if it is established that:</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><strong>а)</strong></span> at the moment of making a bet, a customer had information about the result of the event;</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><strong>b)</strong></span> a customer had an opportunity to influence the result of the event by being a direct participant of the match (sportsmen, referees, coaches, etc.) or a person acting on behalf of a direct participant of the match;</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><strong>c)</strong></span> bets are made by a group of customers who act together (as a syndicate) with the purpose of overcoming specific controls and limits established by the Company;</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><strong>d)</strong></span> one customer has several gaming accounts (multiple registration);</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><strong>e)</strong></span> a customer is suspected of using specialized software or technical means allowing automation of the betting process;</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><strong>f)</strong></span> any untrustworthy means of obtaining information or overcoming the Company-imposed limitations are used;</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;"><span style="border: none;padding: 0in;"><strong>g)</strong></span> any reason where we believe that the account is not being used for genuine betting or gaming purposes.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.26in;"><br></p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">NB: It is each individual customer&rsquo;s responsibility to ensure that the personal details provided at registration are correct. Surebet.ke reserves the right to close, suspend and void, fully or partially, any bets, if the personal details provided are found to be incorrect.</p>
<ul start="11">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">PRICING</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">MNO will charge customers Kshs. 2.5 for each SMS sent / received from surebet.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">The minimum bet amount is Kshs 20 for single bets and multibets.</p>
<ul start="12">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;"><em>ACCEPTANCE AND VALIDATION OF BETS</em></h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">All football bets will be settled on 90 minutes&rsquo; play (also referred to as Full-time and normal time). This denotes the period of play which includes time added by the Referee for injuries and other stoppages. This does not include scheduled extra time, or Penalty shoot-outs, if played.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Where SureBet have not quoted prices on a match, any single bet on that match will be void and treated as a non-runner in multiple bets.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If any team starts a 90-minute competitive game with less than 11 players or where the match does not meet the set requirements by recognized bodies, all bets on that match will be made void.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>LATE BETS</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Bets can only be placed on the outcome of a future event, unless we are offering special &apos;betting in-running prices&apos; for that event. If SureBet mistakenly accepts a bet for an event that has already started then the bet will be void and your stake returned, except in the following circumstances:</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.75in;margin-top: 0.19in;">Where bets are taken after the start of an event, bets will stand if nothing has happened which would significantly alter the chances of the selection winning or losing.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">All bets placed after the result of an event is known will be void win or lose.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Internet bets include bets placed on any of our remote channels including our <u>www.surebet.ke</u> website and via SMS. In the case of such bets, the bet is struck when a bet id or receipt exists in our back end systems or database. We may refuse to take bets placed if they are deemed wrong or mistaken.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>BET CANCELLATION</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">You can cancel a bet within ten(10) minutes after placing that particular bet. This should be before the kick-off time of a match(es) selected in that bet. The maximum number of bets that can be cancelled in a day is three(3). However, any new user cannot cancel their first 3 bets. Live bets CANNOT be cancelled. Bet cancellation is only available on SMS; cancel bet by sending CANCEL#BET ID to 29488.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>ACCOUNT CORRECTIONS</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet has the right to recover from you any amount overpaid and has your authority to adjust your account to reflect the true outcome and rectify the mistake. An example of such a mistake might be where a price is incorrect, a bet is late (see Acceptance and Validation of Bets), where a maximum payout limit has been reached and paid out erroneously or where we enter a result of an event incorrectly.</p>
<ul start="13">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">DISPUTES</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If you have a complaint; you can email customer support on support@risecombiz.com and if your query or complaint is not resolved, you can ask for the matter to be escalated to the Head of Betting Operations. The Head of Betting Operations will investigate and contact you back with a resolution within 48 hours. You will be given the name and status of the people to whom your query/complaint has been referred. If you remain dissatisfied, we may agree with you to refer the matter to an independent third body for arbitration.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet will at all times apply best efforts to resolve a reported matter promptly.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If you have a query with regard to any transaction, you may also contact SureBet at support@risecombiz.com with details of the query. SureBet will review any queried or disputed transactions.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If for some reason you are not satisfied with the resolution of the complaint by the Company, you can complain to the Betting Control and Licensing Board (&quot;BCLB&quot;). Kindly note that by contacting the BCLB you are confirming that you have not breached any of the SureBet Terms and Conditions as agreed to by you upon registration to our service.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">In all other times where a dispute arises, parties shall refer the matter for Arbitration by a single arbitrator agreed by the parities where the chosen venue shall be Nairobi. Where there is no agreement each party shall appoint an arbitrator and the nominated arbitrators shall appoint a single arbitrator and where they are unable to agree the Chairman for the time being of the Kenyan Chapter of Chartered Institute of Arbitrators shall appoint one.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Offensive or rude language, as well as malicious or damaging comments, will not be tolerated while contacting our staff or while discussing our products and services in any media, social network or forum. Any infringement of this policy will result in a suspension of the Account or in every additional action as may be required in order to ensure compliance.</p>
<ul start="14">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">ERRORS</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet makes every effort to ensure that we do not make errors in accepting bets. However, if as a result of human error or system problems a bet is accepted at a price (which includes the odds, handicap provisions, and other terms and conditions or details of the bet) that is:</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">materially different from those available in the general market at the time the bet was made; or clearly incorrect given the chance of the event occurring at the time the bet was made then SureBet will pay winnings at the correct price.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">To establish the correct price SureBet will consider prices available in the general market at the time the bet was made, including the prices offered by other bookmakers operating in the same market.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Examples of circumstances where this would apply are:</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.75in;margin-top: 0.19in;">the price is recorded as 100-1 when the price on offer in the general market is 10-1</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.75in;margin-top: 0.19in;">the margins for handicap betting have been reversed</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If a bet is accepted in error by SureBet on an event or outcome for which no SureBet prices are available, the bet will be void and your stake returned.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">A bet accepted on the website or via SMS does not constitute a legally binding contract between the customer and us. The prices we offer on all outcomes on display on this website take the form of an advertisement rather than a contractual term. If we offer mistaken prices, then we are entitled to decline any bet placed at those prices at any time. In such instances we will void the bet and return the stake money.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Any results or scores displayed on the site for example during betting in play are for guidance purposes only.</p>
<ul start="15">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">FRAUD</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet will seek criminal and contractual sanctions against any customer involved in fraud, dishonesty or criminal acts. SureBet will withhold payment (including but not limited to possible payouts and bet amount) to any customer where any of these are suspected.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">The customer shall indemnify and shall be liable to pay to SureBet, on demand, all costs, charges or losses sustained or incurred by SureBet (including any direct, indirect or consequential losses, loss of profit and loss of reputation) arising directly or indirectly from the customer&rsquo;s fraud, dishonesty or criminal act.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>PRICE CHANGES</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">All prices are subject to change and may, on occasion, be restricted to certain stake levels. The prices offered via our different business channels may vary. For example, our betting shops may offer slightly different prices from those available on this website or available by SMS.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>RESULTS</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">In the case of Football and other Sports, bets are settled on the official result published by the relevant event managers or result handlers 3 to 24 hours after the match/event is finished.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>VOID BET</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">&quot;Void Bet&quot; means the bet is nil or invalid. This occurs when an event is postponed/cancelled, or when it has started but not finished within the period specified in our policy.<br>&nbsp;If a game has been cancelled or postponed there is always a 24 hours wait until the match will be set as void. Once the match has been set as void (with odd 1.00) the rest of the winning ticket will then be paid out.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If a selection in a single bet is made void the stake will be returned.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Void selections in multiple bets will be treated as non-runners and the stake will run onto the remaining selections in the bet.</p>
<ul start="16">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">WITHHOLDING PAYMENT AND OUR RIGHT TO SET OFF LIABILITY</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">We reserve the right to withhold payment and to declare bets on an event void if we have evidence that the following has occurred:</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.75in;margin-top: 0.19in;">the integrity of the event has been called into question</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.75in;margin-top: 0.19in;">the price(s) or pool has been manipulated</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.75in;margin-top: 0.19in;">match rigging has taken place</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Evidence of the above may be based on the size, volume or pattern of bets placed with us across any or all of our betting channels. A decision given by the relevant governing body of the sport in question (if any) will be conclusive. If any customer owes any money to SureBet for any reason, we have the right to take that into account before making any payments to that customer.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>90-MINUTE FOOTBALL BETS</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">What is meant by &apos;90-minute Betting&apos;, &apos;Full Time&apos; and &apos;Normal Time&apos;?</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Adult football matches are played over a 90-minute period, and this period can be called &apos;90-minutes&apos;, &apos;Full Time&apos; or &apos;Normal Time&apos;. All football bets, unless specifically stating they are for Outright, To Qualify, Extra Time or Penalties markets, will be settled on the result after Normal Time (90 minutes).</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">During the game, the referee may stop the game for short periods for substitutions, or if a player gets injured, and at the end of each 45-minute half the referee adds however many minutes he feels is required to account for any of these stoppages. This additional time is called &quot;Injury time&quot;.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Full Time and Normal Time are terms for the full 90-minute period, plus any &apos;Injury Time&apos; that the referee chooses to add. Full Time and Normal Time DO NOT include Extra Time.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Some matches may have a FT period of eighty (80) minutes or LESS as per FIFA regulations. Such matches include; U/16, U/17, Women Seniors etc.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>ABANDONED MATCHES</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If a match is abandoned after it has commenced, all bets on that match will be made void except where settlement has already been determined. For example, where the first goal has been scored by a named player the First Goal scorer and Time of First Goal markets, amongst others, will stand.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>POSTPONED/RE-ARRANGED MATCHES</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">A postponed match is void unless it is re-scheduled to commence within 24 hours of the original start time and this is confirmed within 12 hours of the original start time. In such circumstances where (a) void match(es) is/are included in an accumulator the bet will be settled on the remaining selections.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>PRICES SUBJECT TO FLUCTUATION</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">All prices are subject to fluctuation up to the kick-off. All football bets will be settled using SureBet online ( <a href="http://www.betika.com/"><u>www.surebet.ke</u></a> )prices at the time the bet is placed.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>RELATED CONTINGENCIES IN ONE MATCH</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Where we offer various betting opportunities from the same match (e.g. correct score, first player to score etc.) these cannot be combined in accumulative bets where the outcome is related (except where special fixed prices are available).</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Where an accumulative bet of this type has been accepted in error it will be settled by equally dividing the stake unit where the related outcomes clash.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>RESULTS</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">If the result is not immediately available from an official channel, then the result published by the official governing body immediately the match/event is finished will be used for settling purposes. Any subsequent corrections or amendments to this result will not apply for settlement purposes. SureBet will settle all bets rapidly but because of time differences, for some events, bets may be settled overnight or with some delay.</p>
<ul start="17">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">WITHHOLDING TAX ON WINNINGS</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Please note that all placed amount/successful/won bets/payouts are subject to a 20% withholding tax.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">This means that all our customers/punters will now get less tax on all transactions.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">A successful/won bet/payout constitutes of stake placed plus the additional won amount, jackpot prizes, cash bonuses and any other form of winnings awarded by SureBet. This amount is what will be taxed.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Where odds are less than 1.25, the customer may be subject to receiving winnings, equivalent or lower than the amount staked due to the withholding tax being calculated on staked amount plus winnings.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">This is in compliance with regulatory directives from the Kenya Government, Betting Control and Licensing Board (BCLB), Kenya Revenue Authority (KRA) and the 2018 Finance Act.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>MINIMUM/MAXIMUM BET AMOUNT &amp; PAYOUTS* Items may vary from time to time</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Minimum Bet Amount: The minimum betting amount for a single/multi bet is Kshs 50</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Maximum Bet Amount: The maximum betting amount for a pre-match single bet is Kshs 5,000.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Maximum Bet Amount: The maximum betting amount for a pre-match multi bet is Kshs 5,000.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Maximum Bet Amount for Live bets: The maximum betting amount for a live bet is Kshs 5,000.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Maximum Single Bet Winning: The Single bet winning amount is limited to Kshs 5,000</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Maximum Multi bet Winning: The Multi bet winning amount is limited to Kshs 10,000</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Maximum Aggregate Winning (maximum payout): The Maximum winnings amount per customer per day is limited to Kshs 50,000 unless it&rsquo;s a Grand Jackpot prize or bonus.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Withdraw:&nbsp;</strong>To withdraw, send WITHDRAW#AMOUNT to 29488. You can also withdraw on <a href="http://www.betika.com/"><u>www.surebet.ke&nbsp;</u></a>. Once you are logged in, click on <a href="http://www.betika.com/withdraw"><u>withdraw&nbsp;</u></a>, enter amount and click on &lsquo;withdraw now&rsquo;.Minimum withdrawable amount is Kshs. 50. Maximum withdrawable amount per day is Kshs. 5,000. Kindly note you are not able to withdraw your bonus apart from the Jackpot bonus. Withdrawal charges apply.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>ACCOUNT CLOSURE/FREEZE</h2>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">We reserve the right to close/freeze any customer account or refuse to take bets from any customer. In this event of account closure, the full value of any customer funds will be returned as soon as applicable</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Should you attempt to withdraw funds that were deposited but not used for wagering, the Company may levy a processing fee of 50% upon such withdrawals. Additionally, should these transactions be deemed suspicious, the Company will report the activity to the appropriate authority and the player may lose these funds.</p>
<h2 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Dormant Accounts</h2>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Surebet.ke will deem an account as &ldquo;Dormant&rdquo; if no betting activity has taken place on the account for a minimum of 6 months.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Please note that accounts with a zero balance will be closed.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">If you have any outstanding balance on your Surebet account, you shall receive a communication at least 28 days prior to your account reaching dormant status.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Once your Surebet account is made dormant, should there be an outstanding balance on your account, the said amounts will be surrendered in consultation with BCLB to the relevant Kenya authority.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">If you have any further queries regarding this, please contact our Customer Support team.</p>
<ul start="18">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">BETTING PROCEDURES</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Legal person above 18 years of age can play as many times as they desire but within the daily limits on bet values and payouts set aside in our Terms and Conditions</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Registration<strong>:</strong> SMS &ldquo;JOIN&rdquo; to 29488. You will receive a confirmation message from 29488 confirming that you are now registered and ready to bet. visit <a href="http://www.betika.com/"><u>www4surebet.ke</u></a> to register.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet Account Top-up M-PESA<strong>:&nbsp;</strong>To top up your SureBet Account, go to the Mpesa Menu, Lipa na Mpesa, go to pay bill number enter business number 599488, account no. SUREBET and enter the amount you wish to deposit. You can also deposit online at <a href="http://www.betika.com/"><u>www.surebet.ke</u></a>. Once you&rsquo;ve logged in, click on &lsquo;Deposit&rsquo;, enter amount and then click on &lsquo;Top Up Now&rsquo;.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Balance<strong>:&nbsp;</strong>To check your balance, SMS the word BALANCE to 29488. To check your bonus balance, SMS the word BONUS to 29488. You can also view your balance by login into <u>www.surebet.ke</u>&nbsp;</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-top: 0.19in;">Placing Bets<strong>:</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">The minimum bet amount is and the maximum limit is set in the terms and conditions.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">There are three possible results in any football match:</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">1 - Home Team wins</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">X - Draw result</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">2 - Away Team wins</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">To bet on your team, SMS SUREBET to 29488 and you will receive current football match fixtures and their ODDS. To receive more games simply SMS SUREBET to 29488. The more you SMS the more games you receive.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">You can also visit the SureBet website <a href="http://www.betika.com/"><u>www.surebet.ke</u></a> and identify the game you wish to bet on.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Types of bets:</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Single Bet:&nbsp;</strong>Place bet in this format; GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">e.g. GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">to bet on this match, SMS 123#1#200 to 29488</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Multi Bet:&nbsp;</strong>Place bet in this format; GAMEID#PICK#GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">e.g. GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 456 Ajax Vs Sparta (1= 1.03 X =1.55 2=1.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">to bet on this match, SMS 123#1#456#X#200 to 29488</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Double Chance (DC):&nbsp;</strong>Possible outcomes &ndash; 12, 1X, X2 Pick Format: DC12 DC1X DCX2</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Single Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">E.g. 123#DC1X#200</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 456 Ajax Vs Sparta (1= 1.03 X =1.55 2=1.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Multi Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">E.g. 123#DC1X#456#DC12#200</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Goal Goal (GG)&nbsp;</strong>Both teams score</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>No Goal (NG)&nbsp;</strong>No team scores or only one team scores:</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Single Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">E.g. 123#GG#200</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">123#NG#200</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 456 Ajax Vs Sparta (1= 1.03 X =1.55 2=1.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Multi Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">E.g. 123#GG#456#NG#200</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Over/Under 0.5//1.5/2.5/3.5/4.5/5.5:</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Over 2.5 (ov25)&nbsp;</strong>over 3 goals scored</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Under 2.5 (un25)&nbsp;</strong>under 2 goals scored</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Single Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">E.g. 123#ov25#200 &nbsp; &nbsp; &nbsp; &nbsp; 123#un25#200</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 456 Ajax Vs Sparta (1= 1.03 X =1.55 2=1.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Multi Bet - Place bet in this format: GAMEID#PICK#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">E.g. 123#ov25#456#un25#200</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Halftime/Fulltime</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID#HtFt(Halftime Pick Fulltime Pick)#AMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">123#HtFtX1#100</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Correct Score</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Game ID#ftcspick#Amount</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">e.g. 3214#ftcs23#100</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Draw No Bet</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Picks: DNB1/DNB2</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID#PICKAMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">123#DNB1#100</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>First Half</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Picks: HT1/HT2</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID#PICKAMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">123#HT1#100</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>First Half &ndash; Both Teams to Score</strong></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Picks: HTGG/HTNG</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-top: 0.19in;"><br><br></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID#PICKAMOUNT</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">GAMEID 123 Barcelona Vs Madrid (1= 2.03 X =2.55 2=2.99)</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">123#HTNG#100</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Winners&rsquo; Notification:&nbsp;</strong>Winners will receive an SMS notification of their winnings and have it deposited in their SUREBET account a few minutes after the game ends. The extra minutes allows us to validate the results and post them on our website <a href="http://www.betika.com/"><u>www.surebet.ke</u></a></p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Withdraw:&nbsp;</strong>To withdraw, send WITHDRAW#AMOUNT to 29488. You can also withdraw on <a href="http://www.betika.com/"><u>www.surebet.ke&nbsp;</u></a>. Once you are logged in, click on <a href="http://www.betika.com/withdraw"><u>withdraw&nbsp;</u></a>, enter amount and click on &lsquo;withdraw now&rsquo;. Minimum withdrawable amount is Kshs. 50. Maximum withdrawable amount per day is Kshs. 20,000. Kindly note you are not able to withdraw your bonus apart from the Jackpot bonus. Withdrawal charges apply.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">PROMOTIONAL SMS</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">To stop receiving promotional messages from SUREBET, SMS the word &lsquo;STOP&rsquo; to 29488.</p>
<ul>
    <ul>
        <ul>
            <li>
                <h3 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>BONUS</h3>
            </li>
        </ul>
    </ul>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet bonus scheme is currently/will be available on the following:</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Free Deposit:</strong> Cash back on transaction charges as bonus on first deposit of the day of 99 bob and above.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;"><strong>Referral bonus:</strong> this will be awarded to users who refer their friends to SureBet by sending ACCEPT#FRIEND&rsquo;S NUMBER to 29488. Those referred must be new users and are required to place their first bet with a minimum of Kshs. 40 within 72hrs to enable the referrer earn their bonus. Bonus awarded will match the referred person&apos;s first cash bet amount upto a maximum of Kshs 100. E.g. if the referred user places a fist cash bet of Kshs 50, the user who referred him/her gets a Kshs 50 bonus.The maximum referral bonus one can accrue in a day is Kshs 1,000.</p>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">Bonus awarded cannot be withdrawn. However, bonus winnings derived from the bets placed successfully with the bonus can be withdrawn.</p>
<ul start="19">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">Self check / responsible gambling</h1>
    </li>
</ul>
<ul>
    <ul>
        <ul start="2">
            <li>
                <h3 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Bet History</h3>
            </li>
        </ul>
    </ul>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">You can check your previous bets at any time by logging into the site and viewing the My Account &ndash; Bet History section.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">There are date parameters by which you can filter a search and you can also choose to only view Open or Settled bets.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Once you have made your selection and pressed &apos;Show&apos; a list of qualifying bets will appear. Click on the corresponding line of any bet to see its full details.</p>
<ul>
    <ul>
        <ul start="3">
            <li>
                <h3 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Account History</h3>
            </li>
        </ul>
    </ul>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">You can check your transaction history at any time by logging into the site and viewing the My Account &ndash; Account History section.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">There are date parameters by which you can filter a search.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Once you have made your selection and pressed &apos;Show&apos; a list of all transactions will appear. This will show deposits, withdrawals, and any other account adjustments.</p>
<ul>
    <ul>
        <ul start="4">
            <li>
                <h3 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Login</h3>
            </li>
        </ul>
    </ul>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">For your security in the My Account - Login History you can view all your previous login times and the IP address from which that login was made.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Upon login, the main page automatically displays the length of time you have been logged during the current session.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">All actions while logged in will be performed over a secure connection until you log out or the session is terminated.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">The system will automatically log you out if you have been inactive for a certain length of time. You can change this setting in the My Account &ndash; Other Settings section.</p>
<ul>
    <ul>
        <ul start="5">
            <li>
                <h3 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Password / Login</h3>
            </li>
        </ul>
    </ul>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Should any part of your registered details be construed as offensive in any way (i.e. login) then we reserve the right to close that account immediately.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">In the My Account section you can change your existing password.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">If you have forgotten your password there is a &quot;Lost Password?&quot; link on the main page which explains the steps necessary to recover your password.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">You can use one of three options as your login: account number, username or email depending on your authorisation settings. Your account number and username cannot be changed once the account has been opened.</p>
<ul>
    <ul>
        <ul start="6">
            <li>
                <h3 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Optimal Settings</h3>
            </li>
        </ul>
    </ul>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Although this website will work across all browsers, in order to get the best experience we recommend that you use Google Chrome or Mozilla Firefox on a 1024 x 768 (upwards) screen resolution.</p>
<ul>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Security</p>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">The Surebet.kewebsite uses a secure SSL connection, so you can rest assured that your personal details and card information will always be secured when transmitting your data to and from the server. When the website goes into secure mode, the address bar will change colour and a small padlock icon will be displayed.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">For more information on the certificate used on this site, visit <span style="border: none;padding: 0in;"><u><a href="https://www.comodo.com/" target="_blank">www.comodo.com</a></u></span>.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Additional security features include the last login date information. A detailed breakdown of login details can be accessed by going to My Account and then Login History.</p>
<ul>
    <ul>
        <ul start="7">
            <li>
                <h3 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;'>Cookies and supporting services</h3>
            </li>
        </ul>
    </ul>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">In order to use this website, both cookies and JavaScript must be enabled. To enable cookies on your browser, please follow the guide provided on:<br><span style="border: none;padding: 0in;"><u><a href="http://www.google.com/cookies.html" target="_blank">http://www.google.com/cookies.html</a></u></span></p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">In order to enable JavaScript please follow the steps provided:<br><span style="border: none;padding: 0in;"><u><a href="http://support.microsoft.com/gp/howtoscript" target="_blank">http://support.microsoft.com/gp/howtoscript</a></u></span></p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">It should be noted that no sensitive data will ever be stored in our cookies.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">Accepting Surebet Rules, you agree with&nbsp;the support to ensure access&nbsp;to Surebet services.</p>
<ul start="19">
    <li>
        <h1 style='margin-top: 0in;margin-bottom: 0in;line-height: 100%;text-align: left;font-family: "Arial, Helvetica, sans-serif">Surebet Mobile</h1>
    </li>
</ul>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">How do I use the Surebet.ke mobile version?</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">If you decide to use Surebet.ke mobile version in order to place a bet, you will need to use your login and password that you chose when opening your Surebet account. If you cannot remember your password, then please use the &ldquo;Lost Password?&rdquo; link on our main page where you will find a guide to reset your password.</p>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">Which mobile browsers are compatible with the Surebet.ke mobile version?</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">In order to find out whether your device supports the Surebet.ke mobile version, all you need to do is to enter the Surebet.ke website address into the browser pre-installed on your device. After that you will be automatically redirected to the website version that is supported by your device.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">At the moment the mobile.Surebet.ke website supports the following browsers:</p>
<ul>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Mozilla</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Chrome</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Default Android</p>
    </li>
    <li>
        <p style="margin-bottom: 0in;line-height: 100%;text-align: left;">Opera</p>
    </li>
</ul>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.75in;"><br></p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.75in;"><br></p>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">What is the Surebet mobile app? Coming soon.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Surebet mobile app is a multifunctional mobile application which provides customers with instant access to the best prices for all types of sports. Customers can quickly and easily place bets directly from their smartphones or tablets using the &ldquo;Quick Bet&rdquo; option. In addition to an extensive selection of sports and markets, the Surebet mobile app offers its users a wide range of LIVE events&rsquo; prices which are updated in real time. Therefore, whatever sport you are interested in, you will always have the best prices at your fingertips.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;"><br></p>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">Which platforms does the Surebet mobile app work on?</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;">The Surebet mobile app is compatible with all mobile devices using iOS (version 8.1 and above) and Android (version 4.2.2 and above). If our mobile app is not compatible with your device, you can use our mobile site via your mobile browser.</p>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">Are all of the events listed on the Surebet.ke website available on the mobile version?</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">The majority of sporting events represented on the Surebet.ke website are available for the mobile version and Surebet mobile app.</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;"><br></p>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">Which payment methods are available for use on the mobile version?</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">At the moment most payment systems on our main website Surebet.ke are also available in the Surebet mobile app. To use a payment method that is not available in the Surebet mobile version, please visit our main website Surebet.ke</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;"><br></p>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">Is the Surebet mobile app free to use?</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Surebet does not charge for downloading or using this app. The Surebet mobile app for iOS devices is available in the App Store. The app for Android is available for download directly from the Surebet.ke website. The only charge in the course of using our app will be your standard mobile operator&rsquo;s charge</p>
<p style="margin-bottom: 0.11in;line-height: 108%;text-align: left;">Can I use the Surebet mobile app abroad?</p>
<p style="margin-bottom: 0in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.13in;">Yes, you can use the Surebet mobile app abroad, as long as your mobile operator provides roaming services and GPRS data transfer. Please clarify the data transfer conditions abroad with your mobile operator. The Surebet mobile app can also be used abroad via Wi-Fi.</p>
<ul start="19">
    <li>
        <h1 style='margin-top: 0.03in;margin-bottom: 0in;text-align: left;font-size: 14px;">ALTERATION OF THE WEBSITE</h1>
    </li>
</ul>
<p style="margin-bottom: 0.19in;line-height: 100%;text-align: left;margin-left: 0.25in;margin-top: 0.19in;">SureBet may, in our absolute discretion, alter or amend any product or service (including any prices offered) available through the website ( <a href="http://www.betika.com/"><u>www.surebet.ke</u></a>) at any time for the purpose of ensuring the ongoing provision of the website, but without prejudice to any games and/or bets already in progress at the time of such amendment. From time to time, we may restrict you from accessing some parts of the website for the purpose of maintenance of the website and/or alteration or amendment of any of the games and/or products available through the website.</p>

