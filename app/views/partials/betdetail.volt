<table class="betdetail">
  <tr class="title">
    <th class="text-left id"> TIP ID : <?= $myTip['user_tip_id'] ?></th>
    <th class="text-right status pending dark-gray"><?php
       if($myTip['status']==5){
           echo '<span class="won">Won</span>';
       }elseif($myTip['status']==4){
          echo 'Cancelled';
       }else{
          echo 'View';
       }
       ?></th>
  </tr>
  <tr>
    <td colspan="10">
      <table class="summary">
        <th>Date</th>
        <th>Tip Amount</th>
        <th>Total Tips Odds</th>
        <tr>
          <td><?= date('d/m H:i', strtotime($myTip['created'])) ?></td>
          <td><?= $myTip['tip_amount'] ?></td>
          <td><?= $myTip['total_odd'] ?></td>
        </tr>
      </table>
      <span class="pull-left padding-up-down"> Tip Details </span>
      <?php foreach($tipDetails as $tip): ?>
      <table class="event">
        <th colspan="10" class="text-left">
          <span class="dark-gray">#<?= $tip['game_id'] ?></span> : <?= $tip['home_team']." v ".$tip['away_team'] ?></th>
        
        <tr class="detail">
          <td class="border-right border-bottom">
            <table>
              <tr>
                <td class="text-left">
                  <span class="dark-gray">Date</span>
                </td>
                <td class="text-right">
                  <span class="theme-color"><?= date('d/m H:i', strtotime($tip['start_time'])) ?></span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="detail">
          <td class="border-right border-top">
            <table>
              <tr>
                <td class="text-left">
                  <span class="dark-gray">TIP</span>
                </td>
                <td class="text-right" style="font-weight: bold;color: #27357E;">
                  <span class="theme-color"><?= $tip['prediction'] ?></span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="detail">
          <td class="border-left border-top">
            <table>
            <tr>
              <td class="text-left">
                <span class="dark-gray">Tip Odds</span>
              </td>
              <td class="text-right">
                <span class="theme-color">
                  <?= $tip['odd_value'] ?>
                </span>
              </td>
            </tr>
          </table>
        </td>
        </tr>
        <tr class="detail">
          
          <td class="border-left border-top">
              <table>
              <tr>
                <td class="text-left">
                  <span class="dark-gray">Results</span>
                </td>
                <td class="text-right">
                  <span class="theme-color"><?= $tip['results'] ?></span>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <?php endforeach; ?>

    </td>
  </tr>
</table>