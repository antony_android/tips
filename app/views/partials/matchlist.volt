
<?php 
      function clean($string) {
         $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
         $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

         return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
      }
      $empty_row_text = '<table cellspacing="0" cellpadding="0"> <tr> <td class=""><button  class="odds-btn"><span class="odd" style="opacity:0.3"><img height="15" width="15" src="/img/padlock.svg" alt="-" /></span></button></td> </tr> </table>';

?>
<table class="highlights" width="100%">
        <tr>
          <td style="padding: 0;">
               <table width="100%" cellpadding="0" cellspacing="0" class="mkt-headers">
                       <tbody>
                          <tr>
                             <td width="50%" style="float:left; text-align:left;">
                              GAMES
                           </td>
                       <td width="50%">
                           <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                             <td width="28%">1</td>
                             <td width="28%">X</td>
                             <td width="28%">2</td>
                             <td width="16%"></td>
                           </tr>
                           </table>
                       </td>
                       </tr>
                   </tbody>
               </table>
          </td>
        </tr>

        <tr>
          <td style="padding: 0;">

         <!-- List matches -->

          <?php foreach($matches as $day): ?>

            <?php $theMatch = @$betslip[$day['match_id']]; 
            ?>

            <table class="highlights--item" width="100%" cellpadding="0" cellspacing="0">
              
              <tr>
                <td >
                  <table class="league">
                    <tr >
                      <td style="text-align: left; vertical-align:top;" class="meta" width="50%">
                          <table>
                              <tbody>
                              <tr>
                                 <td>
                                    <?php echo $day['competition_name']; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="team-names">
                                     <?php echo strtoupper($day['home_team']); ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="team-names">
                                     <?php echo strtoupper($day['away_team']); ?>
                                 </td>
                              </tr>

                              </tbody>
                          </table>
                       </td>
                       <td  width="50%" style="text-align: right;">
                          <table width="100%">
                              <tbody>
                              <tr>
                                 <td>
                                    <?php echo date('d/m H:i', strtotime($day['start_time'])); ?> - <?php echo $t->_('Game ID'); ?>: <?php echo $day['game_id']; ?>
                                 </td>
                              </tr>
                              <tr>
                              <td width="28%">
                                 <table class="real-odds" width="100%">
                                     <tr class="odds">
                                        <td class="clubone">
                                          <?php if($day['home_odd']) { ?>
                                          <table cellspacing="0" cellpadding="0">
                                            <tr>

                                              <td class="">
                                                 <button>
                                                    <span class="odd"><?php echo $day['home_odd']; ?></span></button></td>

                                            </tr>
                                         </table>
								      <?php } else { ?>
                                               <?php echo $empty_row_text; ?>
									  <?php } ?>
                             </td>
                             <td width="28%" class="">
                                          <?php if($day['draw_odd']) { ?>
                                            <table>
                                             <tr>
                                                    <td class="">
                                                       <button>
                                                          <span class="odd"><?php echo $day['draw_odd']; ?></span>
                                                         </button>
                                                      </td>
                                                    
                                              </tr>
                                           </table>
								      <?php } else { ?>
                                               <?php echo $empty_row_text; ?>
									  <?php } ?>

                              </td>
                              <td class="clubtwo" width="28%">
										  
                                          <?php if($day['away_odd']) { ?>
                                <table>
                                   <tr>
                                              <td class="">
                                                 <button><span class="odd"><?php echo $day['away_odd']; ?></span></button></td>
                                              
                                 </tr>
                              </table>
									<?php } else { ?>
                                           <?php echo $empty_row_text; ?>
									 <?php } ?>

                           </td>
                           <td class="sidebet" width="16%">
                              <button style="background: #FFD800;" class="tippy tip-<?php echo $day['game_id']; ?>" href="javascript:;" hometeam="<?php echo $day['home_team']; ?>" awayteam="<?php echo $day['away_team']; ?>" target="javascript:;" id="<?php echo $day['game_id']; ?>" onClick="addTip(this.id,this.getAttribute('hometeam'),this.getAttribute('awayteam'))">ADD</button>      
                           </td>

                                      </tr>

                                 </table> <!-- end table real-odds -->

                                 </td>
                              </tr>

                              </tbody>
                          </table>

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>

            <?php endforeach; ?>

        <!-- List matches end -->

          </td>
        </tr>
        <?php if($pages > 1): ?>
             <tr class="pagination">
              <td style="text-align:left">
                <table style="float:left">
                  <tr>
                  <?php if($page > 1): ?>
                  <td class=""><a href="?page=<?= $page-1; ?>">< </a></td>
                  <?php endif; ?>
                  <?php for ($x = 0; $x <= $pages-1; $x++): ?> 
                    <td class="<?php if($x==$page){ echo 'selected';} ?>"><a href="?page=<?= $x; ?>" ><?= $x+1; ?></a></td>
                    <?php endfor; ?>
                    <?php if($page == $pages): ?>
                    <td class=""><a href="?page=<?= $page+1; ?>">> </a></td>
                    <?php endif; ?>
                  </tr>
                </table>
              </td>
        </tr>
<?php endif; ?>     
      </table>
