<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8L6KWL40KX"></script>
    <script>
	  window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'G-8L6KWL40KX');
	</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $t->_('description'); ?>">
    <meta name="keywords" content="<?php echo $t->_('keywords'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:site_name" content="<?php echo $t->_('app_name'); ?>"/>
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta property="" content="<?php echo $t->_('app_name'); ?>"/>
	
    <title><?php echo $t->_('title'); ?></title>

    {{ stylesheet_link('css/lite.css?v=1.0.14') }}

    <style type="text/css">
        .brand, .brand .betslip {
            background-color: #27323E !important;
        }

        .betslip a {
            color: white !important;
        }

        .betslip .betslip--count {
            color: black !important;
            background-color: #fbd702 !important;
            margin-left: 5px;
        }
    </style>
</head>
