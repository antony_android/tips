<?php $totalOdds=1; ?>
<table width="100%" class="betslip top">
                <th class="title" colspan="10">SELECTED GAMES - TIPS SLIP</th>
                <tr>
                  <td colspan="10">
                    {{ this.flashSession.output() }}
              <?php
                $matchCount = 0;
                $bonus = 0;
                $bonusOdds = 1;
              ?>

              <?php foreach((array)$betslip as $bet): ?>
                <?php
                  if (!$bet){continue;}
                  $odd = $bet['odd_value'];

                  $totalOdds = round($totalOdds*$odd,2);

                  ?>
                    <table class="bet">
                      <tr>
                        <td class="padding-up-down" colspan="10">
                          <table width="100%">
                            <tr>
                              <td>
                                <table>
                                  <tr class="game">
                                    <td>

                                    <?php

                                    if($bet['away_team']=='na'){
                                      echo $bet['home_team']; 
                                      }
                                      else{
                                        echo $bet['home_team']." v ".$bet['away_team'];
                                      }

                                    ?>


                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td class="odd-del">
                                <table style="width:100%;text-align:center;">
                                  <tr>
                                  <?php echo $this->tag->form("tipslip/remove?bs=1"); ?>
                                    <td class="delete">
                                    <input type="hidden" name="match_id" value="{{bet['match_id']}}">
                                    <button class="remove-match" type="submit" value="submit">X</button>
                                    </td>
                                  </form>
                                  </tr>
                                  <tr>
                                    <td></td>
                                  </tr>
                                  <!-- <tr>
                                    <td class="odd"><?php echo $bet['odd_value']; ?></td>
                                  </tr> -->
                                 
                                </table>
                              </td>

                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                <?php endforeach; ?>

                  </td>
                </tr>
				</table>

        <?php if(count($betslip)){ ?>
        <!-- begin betslip bottom fixed pane -->
        <table width="100%" class="betslip full-width" style="position:fixed; overflow:hidden; bottom:0; max-width:768px; background:#fff; border-top:1px solid #e3e3e3;">
        <tr class="details" style="">
          <td class="right">
              
              <?php echo $this->tag->form("tipslip/checkout"); ?>
              <table width="100%">
                {% if session.get('auth') == null %} 
                <tr>
                  <td class="dark-gray">Phone Number</td>
                  <td><input type="text" id="msisdn" class="stake" name="msisdn" value=""></td>
                </tr>
                {% endif %}
                <tr>
                  <td class="dark-gray">Amount to Pay</td>
                  <td><input type="number" id="stake_amount" class="stake" readonly name="stake" value="{{stake}}"></td>
                </tr>

                <tr>
                  <td colspan="9">
                  {% if session.get('auth') != null %} 
                  <input type="hidden" name="msisdn" value="{{session.get('auth')['mobile']}}">
                  <input type="hidden" id="user_id" name="user_id" value="{{session.get('auth')['id']}}">
                  {% endif %}
                  <input type="hidden" name="src" value="mobile">
                  <input type="hidden" id="user_id" name="user_id" value="">
                  <input type="hidden" id="total_odd_m" name="total_odd" value="<?php echo $totalOdds; ?>">
                  
                  <button type="submit" class="place" style="margin-bottom: 15px;" onclick="fbPurchase()">GET TIPS</button>
                  <!-- {% if session.get('auth') == null %}  -->
                  <!-- <a href="{{url('login')}}?ref=tipslip" class="place dark-gray login-button">GET TIPS</a> -->
                  <!-- {%  endif %} -->
                  </td>
                </tr>
              </table>

              </form>
            </td>
        </tr>
        <tr class="spacer"></tr>
        <tr>
          <td class="" colspan="10" style="background-color:#E8E8E8">
        <?php echo $this->tag->form("tipslip/clearslip"); ?>
        <input type="hidden" name="src" value="mobile">
          <button type="submit" class="delete-all">Delete All</button>
        </form>
          </td>
        </tr>
      </table>

      <!-- end slip footer -->
      <script type="text/javascript" >
        function updateWinning(){
            var bamount = document.getElementById("stake_amount").value;
            var total_odd = document.getElementById("total_odd_m").value;
            var raw_win = bamount*total_odd;
            var js_tax = (raw_win -bamount)*0.2;
            var net_wi = raw_win -js_tax;
            var poss_id = document.getElementById("possible_win_id");
            poss_id.innerHTML = raw_win.toFixed(2);

            var tax_id = document.getElementById("tax_id")
            tax_id.innerHTML = js_tax.toFixed(2)


            var new_win_id = document.getElementById("net_win_id")
            net_win_id.innerHTML = net_wi.toFixed(2);
            return false;

        }
      </script>
      <?php } ?>