<!-- <table class="football">
<tbody>
    <tr><th class="title" colspan="2"><?= $current_sport['sport_name'] ?></th></tr>

</tbody>
</table>
-->
<table class="top--nav" width="100%">
    <tr>
        <td class="<?= ($tab == 'today') ? 'selected': ''; ?>">
            <a href="{{ url('') }}?id={{current_sport['sport_id']}}">Today</a>
        </td>
        <td class="<?= ($tab == 'highlights') ? 'selected': ''; ?>">
            <a href="{{ url('highlights') }}?id={{current_sport['sport_id']}}"><?php echo $t->_('highlights'); ?></a>
        </td>
        <td class="<?= ($tab == 'tomorrow') ? 'selected': ''; ?>">
            <a href="{{ url('tomorrow') }}?id={{current_sport['sport_id']}}"><?php echo $t->_('tomorrow'); ?></a>
        </td>
    </tr>
</table>
