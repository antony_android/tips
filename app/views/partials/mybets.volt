<table class="mybets">
  <th class="title">My Tips (Paid)</th>
  <?php foreach($myTips as $tip): ?>
  <tr class="bet">
    <td>
      <a href="{{ url('mytips/show?id=') }}{{tip['user_tip_id']}}" class="undecorate">
      <table>
        <tr class="title">
          <td class="id text-left dark-gray">Tip ID: <span class="theme-color"><?= $tip['user_tip_id'] ?></span></td>
          <td class="status text-right"><?php
        if($tip['xstatus']==5){
          echo '<span class="won">Won</span>';
        }elseif($tip['xstatus']==4){
        echo 'Cancelled';
        }else{
        echo 'View';
        }
       ?></td>
        </tr>
        <tr class="detail">
          <td class="border-right border-bottom"><span class="pull-left dark-gray">Date</span> </td>
          <td>
            <span class="pull-right theme-color"><?= date('d/m H:i', strtotime($tip['created_at'])) ?></span>
          </td>
        </tr>
      </table>
      </a>
    </td>
  </tr>
  <?php endforeach; ?>

</table>
