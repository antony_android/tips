<?php 
  function clean($string) {
     $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
     $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

     return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
  }
?>
<table class="jp">
  <th class="title"> 1X2 JACKPOT <br> 
  <span class="meta">{{startTime}}</span>
  <div class='alert alert-info alert-dismissible'>Jackpot bet for KSH250 </div>
  </th>
  <tr>
    <td>
      <table class="highlights" width="100%">
        <tr>
          <td style="padding: 0;">
{{ this.flashSession.output() }}
            <!-- List matches -->

          <?php foreach($games as $day): ?>

            <?php
               $theMatch = @$theBetslip[$day['match_id']];
               $odds = $day['threeway'];
               $odds = explode(',',$odds);
               $home_odd = $odds['0'];
               $neutral_odd = $odds['2'];
               $away_odd = $odds['1'];
            ?>

            <table class="highlights--item" width="100%" cellpadding="0" cellspacing="0">
              
              <tr class="game">
                <td colspan="10">
                  <table width="100%">
                    <tr>
                      <td class="clubs" colspan="2"><?php echo $day['pos']; ?>. <?php echo $day['home_team']; ?> V. <?php echo $day['away_team']; ?></td>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>
                <td colspan="10">
                  <table class="league">
                    <tr >
                      <td style="text-align: left;" class="meta"><?php echo date('d/m H:i', strtotime($day['start_time'])); ?> - Game ID: <?php echo $day['game_id']; ?></td>
                    </tr>
                  </table>
                </td>
              </tr>
          
              <tr class="odds">
                <td class="clubone <?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']);
                     if($theMatch['bet_pick']==$day['home_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                  ?>">
                  <table cellspacing="0" cellpadding="0">
                    <tr>

                      <td class=""><button href="javascript:;" class="" pos="<?= $day['pos']; ?>" hometeam="<?= $day['home_team']; ?>" oddtype="3 Way" bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $home_odd; ?>" target="javascript:;" odd-key="<?php echo $day['home_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['home_team']); ?>" sub-type-id="1" special-value-value="0" onClick="addBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))"><span class="pick">1 </span><span class="odd"><?php echo $home_odd; ?></span></button></td>
                      

                    </tr>
                  </table>
                </td>
                <td class="border-td"></td>
                <td class="draw <?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].'draw');
                     if($theMatch['bet_pick']=='draw' && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                  ?>">
                  <table>
                    <tr>
                      <td>
                        <table>
                          <tr>
                            <td class=""><button href="javascript:;" class="" hometeam="<?php echo $day['home_team']; ?>" pos="<?= $day['pos']; ?>" oddtype="3 Way" bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $home_odd; ?>" target="javascript:;" odd-key="draw" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id']."draw"); ?>" sub-type-id="1" special-value-value="0" onClick="addBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))"><span class="pick">x </span><span class="odd"><?php echo $neutral_odd; ?></span></button></td>
                            
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <td class="border-td"></td>
                <td class="clubtwo <?php echo $day['match_id']; ?> <?php
                  echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']);
                     if($theMatch['bet_pick']==$day['away_team'] && $theMatch['sub_type_id']=='1'){
                        echo ' picked';
                     }
                  ?>">
                  <table>
                    <tr>
                      <td class=""><button href="javascript:;" class="" pos="<?= $day['pos']; ?>" hometeam="<?php echo $day['home_team']; ?>" oddtype="3 Way" bettype='jackpot' awayteam="<?php echo $day['away_team']; ?>" oddvalue="<?php echo $home_odd; ?>" target="javascript:;" odd-key="<?php echo $day['away_team']; ?>" parentmatchid="<?php echo $day['parent_match_id']; ?>" id="<?php echo $day['match_id']; ?>" custom="<?php echo clean($day['match_id'].$day['sub_type_id'].$day['away_team']); ?>" sub-type-id="1" special-value-value="0" onClick="addBet(this.id,this.getAttribute('sub-type-id'),this.getAttribute('odd-key'),this.getAttribute('custom'),this.getAttribute('special-value-value'),this.getAttribute('bettype'),this.getAttribute('hometeam'),this.getAttribute('awayteam'),this.getAttribute('oddvalue'),this.getAttribute('oddtype'),this.getAttribute('parentmatchid'),this.getAttribute('pos'))"><span class="pick">2 </span><span class="odd"><?php echo $away_odd; ?></span></button></td>
                      
                    </tr>
                  </table>
                </td>
              </tr>
            </table>

            <?php endforeach; ?>
          

          </td>
        </tr>
        <tr class="spacer"></tr>
      </table>
    </td>
  </tr>
</table>
<div class="placebet">
<?php echo $this->tag->form("betslip/betJackpot"); ?>
<input type="hidden" id="user_id" name="user_id" value="{{session.get('auth')['id']}}">
             <input type="hidden" name="jackpot_type" id="jackpot_type" value="13" >
             <input type="hidden" name="src" id="src" value="mobile" >
             <input type="hidden" name="jackpot_id" id="jackpot_id" value="{{jackpotID}}" >
 <div class="total-stake"><span class="met">Total Stake: </span><span class="stake-amt">KSH 250</span></div>

 

<!-- if session.get('auth') != null %} -->
<button type="submit" id="place_bet_button" class="place" onclick="fbJackpot()">Place Bet</button>
 <!-- else %}
<a href="{{url('login')}}?ref=jackpot" class="place dark-gray login-button">Login to Bet</a>
  endif %} -->
 </form>
</div>
