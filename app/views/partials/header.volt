<table width="100%" class="brand brandt" style="background-color:#76B729!important; position: sticky; top: 0px;">
	<tr>
	  <td>
		<table style="width: 100%;">
		  <tr>
			  <td class="logo">
          <a href="{{ url('') }}" style="text-decoration: none;
          color: #FFF;
          font-weight: 600;
          font-size: 18px;">
             <img src="{{ url('/img/bt-logo.png') }}" alt="logo">
             <!-- BETIKA TIPS -->
          </a>
        </td>
			  <td class="" style="background-color:#76B729!important; font-size: 10px;">
					Till Number <strong>731356</strong> <br>
          <a href="/deposit" style="color: #fff;background: #5A5A5A;border: 1px solid #991;border-radius: 2px;padding: 2px 5px;display: block;width: max-content;text-decoration: none;margin-top:2px;">DEPOSIT NOW</a>
		
	  </td>
	  <td class="betslip" style="background-color:#76B729!important;">
	    {% if session.get('auth') == null %}
		   <a href="/login" style="text-decoration: none;">Login</a>
	       <a href="/signup" style="background-color: #FBD703; padding:5px; border-radius:4px; margin-left:5px; color: black!important; text-decoration: none;">Register</a>
		   <div style="padding: 5px 0px;">
		   	<a href="/resetpassword" style="text-decoration: none;color: #FBD703 !important;font-style: italic;">Forgot Password?</a>
		   </div>
		{% else %}
			HELP 0792045521 <br/>
		   <a href="/myaccount" style="text-decoration: none;">Account</a> | <a href="/mytips" style="text-decoration: none;">My Tips</a>
		{% endif %}
		</td>
		  </tr>
		</table>
	  </td>
	</tr>
  </table>

<table id="header">
  <tr>
    <td>


	  <table class="landing" style="width: 100%; max-width: 768px; background-color: #fff!important;">
		  <tr class="top--search">
			  <td style="width: 50%;">
				<div style="padding: 2px;">
          <a href="/index" style="color: #fff; font-size: 11px; background-color: #76B729; padding: 5px 8px 5px 8px; border-top-left-radius: 4px; border-bottom-left-radius: 4px; float:left;">
            <img width="16" height="16" src="{{ url('/img/home.png') }}" alt="Home">
          </a>
          <a href="/howitworks" style="color: #fff; font-size: 11px; background-color: #76B729; padding: 8px 8px; margin-left:1px; float:left">How it Works</a>
          <!-- <a href="/live-all" style="color: #fff; font-size: 11px; background-color: #27323E; padding: 8px 8px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; margin-left:1px; float:left">Live</a> -->
				</div>
			</td>
			  <td>
				{{ partial("partials/search") }}
			  </td>
		  </tr>
	  </table>
	  
	 <!--logged in   if session.get('auth') != null-->
		

		<!--  myaccount  mybets  logout -->

      <table width="100%" class="nav-none" style="display: none;">
        <tr>
          <td ><a class="<?= (@$men == 'home') ? 'selected': ''; ?>" href="{{ url('') }}"><?php echo $t->_('Home'); ?></a></td>
          <td ><a class="<?= (@$men == 'live') ? 'selected': ''; ?>" 
            style="padding:5px; background:red; color:#ffffff;" href="{{ url('liveall') }}"><?php echo $t->_('Live'); ?></a></td> 
          <td><a class="<?= (@$men == 'football') ? 'selected': ''; ?>" href="{{ url('football') }}"><?php echo $t->_('Football'); ?></a></td>
          <td><a class="<?= (@$men == 'sports') ? 'selected': ''; ?>" href="{{ url('sports') }}"><?php echo $t->_('Sports'); ?></a></td>
          <td><a class="<?= (@$men == 'jackpot') ? 'selected': ''; ?>" href="{{ url('jackpot') }}">Jackpot</a></td>
        </tr>
      </table>
    </td>
  </tr>
</table>