<!-- remove this footer --
<table id="contacts">
    <tr>
        <td>
            <p><span><?php echo $t->_('M-pesa Paybill'); ?>: </span><?php echo $t->_('paybill'); ?></p>
            <p><span><?php echo $t->_('Account'); ?>: </span><?php echo $t->_('app_name'); ?></p>
            <p><span><?php echo $t->_('Contacts'); ?>: </span>
                <?php echo $t->_('contacts-list') ?>
            </p>
        </td>
    </tr>
</table>

remove footer shiet-->
<?php $slipDetail = strpos($_SERVER['REQUEST_URI'], 'mytips/show') ?>
<?php if(!in_array($_SERVER['REQUEST_URI'], ['/tipslip', '/login', '/logout', '/reset-code', '/forgot-password', '/terms', '/signup', '/verify', '/myaccount', '/deposit', '/withdraw', '/mytips']) && !$slipDetail){ ?>
<form name="terminalbet" action="/tipslip/checkout" method="POST" >
<table style="position:fixed; bottom:0; padding:5px; background-color:#27323E; max-width:768px;"  class="full-width">
    <tr>
        <td style="width:25%;  padding:5px; font-size:12px;  color:white; text-align:center;">
         <a href="/tipslip" style="text-decoration: none; color: unset; background-color:#76B729; padding: 10px 10px; border-radius:4px;">Slip <span id="betslip-count" class="betslip--count slip-counter" style="background-color:#fbd702; color:black; padding:3px 6px; border-radius:224px; ">{% if betslip %}<?= count($betslip); ?> {%else%}0{%endif%}</span></a>  
        </td>
        <td style="background-color:#27323E; width:25%; font-size:12px; text-align:center;"><span style="color:#FFF;">Amount</span><br/>
          <input id="stake" type="text" name="stake" readonly value="{% if betslip %}<?= count($betslip); ?> {%else%}0{%endif%}" style="max-width:50px;" onkeyup="updateWinning()">
          <input id="total_odd_m" type="hidden" name="total_odd" value="{{betslip_data['total_odd']}}" style="max-width:50px;">
          <input type="hidden" name="msisdn" value="{% if session.get('auth') %}{{session.get('auth')['mobile']}}{% endif %}">
          <input type="hidden" name="src" value="mobile">
          <input type="hidden" id="user_id" name="user_id" value="{% if session.get('auth') %}{{session.get('auth')['id']}}{% endif%}">

        </td>
        <!-- <td style="background-color:#27323E; width:25%; color:white; font-size:12px;" > 
            Odds: <span id="total_odd"><?= number_format($betslip_data['total_odd'], 2) ?></span>
        </td> -->
        <td style=" width:25%;  font-size:12px; padding:5px; text-align:center" > 
            {% if session.get('auth') != null %}
                <button type="submit" class="place" onclick="fbPurchase()">GET TIPS</button>
            {% else %}
                <a  href="{{url('tipslip')}}" class="place dark-gray login-button">GET TIPS</a>
             {% endif %} 

        </td>
    </tr>
</table>
</form>
<script type="text/javascript" >
    function updateWinning(){
       var bamount = document.getElementById("stake").value;
       var total_odd = document.getElementById("total_odd_m").value;
       var raw_win = bamount*total_odd;
       var js_tax = (raw_win -bamount)*0.2;
       var net_wi = raw_win -js_tax;
       var poss_id = document.getElementById("possible_win_id");
       poss_id.innerHTML = raw_win.toFixed(2);

       /*var tax_id = document.getElementById("tax_id")
       tax_id.innerHTML = js_tax.toFixed(2)


       var new_win_id = document.getElementById("net_win_id")
       net_win_id.innerHTML = net_wi.toFixed(2);
       */
       return false;

    }
  </script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-N361NKDNJY"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-N361NKDNJY');
</script>

<?php } else if( $slipCount == 0){ ?>

<table id="footer">

    <tr class="top">
        <td>
            <a href="{{ url('#') }}"><?php echo $t->_('policy'); ?></a>
        </td>
    </tr>
    <tr class="spacer"></tr>
    <tr>
        <td>Strictly for persons over 18yrs only <br/> Help 0792 045 521</td>
    </tr>
    <tr class="spacer"></tr>
    <tr class="social-icons">
        <td>
            <ul>
                <li>
                    <a href="https://www.facebook.com/betikatips" target="_blank">
                        <svg viewBox="0 0 512 512">
                            <path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/>
                        </svg>
                        <!--[if lt IE 9]><em>Facebook</em><![endif]-->
                    </a>
                </li>
                <li>
                    <a href="https://www.twitter.com/betikatips" target="_blank">
                        <svg viewBox="0 0 512 512">
                            <path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/>
                        </svg>
                        <!--[if lt IE 9]><em>Twitter</em><![endif]-->
                    </a>
                </li>
                <li>
                <a href="https://www.instagram.com/betikatips" target="_blank">
                    <i class="fa fa-instagram"><img width="20" height="20" style="opacity:0.4;" src="https://en.instagram-brand.com/wp-content/themes/ig-branding/prj-ig-branding/assets/images/ig-logo-black.svg" alt="Inst" /></i>
                </a>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    
</table> 
<?php } ?>