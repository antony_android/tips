<table class="deposit">
  <th class="title">Deposit</th>
  <tr>
    <td style="padding: 5px;">
      <p>
        Enter amount below, use your service pin to authorize the transaction. If you do not have a service pin, please follow the instructions to set.
      </p>
      {{ this.flashSession.output() }}
    </td>
  </tr>
  <tr>
    <td style="padding: 5px;">
        <table class="form">
        <?php echo $this->tag->form("deposit/topup"); ?>

          {% if session.get('auth') == null %} 
          <tr class="input">
            <td>
              <div>
                <label for="msisdn">Phone Number *</label>
                <input type="text" id="msisdn" name="msisdn" value="">
              </div>
            </td>
          </tr>
          {% else %}
          <input type="hidden" name="msisdn" value="{{session.get('auth')['mobile']}}">
          {% endif %}
          <tr class="input">
            <td>
              <div>
                <label for="amount">Amount (KES) *</label>
                <input type="number" name="amount" placeholder="KES">
              </div>
            </td>
          </tr>
          <tr class="spacer"></tr>
          <tr class="input">
            <td>
              <button type="submit" style="background: #27357E;" onclick="fbDeposit()">Top Up Now</button>
            </td>
          </tr>
      </form>
        </table>
      </form>
    </td>
  </tr>
</table>
