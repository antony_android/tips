<?php

class SignupController extends ControllerBase
{
	public function initialize()
	{
		$this->tag->setTitle('Sign up');
	}

	public function indexAction()
	{

	}

	public function joinAction(){

		if ($this->request->isPost()) {

			$mobile = $this->request->getPost('mobile', 'int');
			$terms = $this->request->getPost('terms', 'int');
			$password = $this->request->getPost('password');
			$repeatPassword = $this->request->getPost('repeatPassword');

			if (!$mobile || !$password || !$repeatPassword || !$terms ) {
				$this->flashSession->error($this->flashError('All fields are required'));
				$this->response->redirect('signup');

				// Disable the view to avoid rendering
				$this->view->disable();
			}else{

				if ($password != $repeatPassword) {
					$this->flashSession->error($this->flashError('Passwords do not match'));
					$this->response->redirect('signup');

					// Disable the view to avoid rendering
					$this->view->disable();
					return;
				}

				$mobile = $this->formatMobileNumber($mobile);
				$password = $this->security->hash($password);

				if (!$mobile) {
					$this->flashSession->error($this->flashError('Invalid mobile number'));
					$this->response->redirect('signup');

					// Disable the view to avoid rendering
					$this->view->disable();
				}else{

					// $sms = "Welcome to Betika Tips.\nYour account has been created.\TILL NUMBER: 731356\nwww.betika.tips\nFor help call 0792 045 521.";

					// $sms = array(
					// 	'msisdn' => $mobile,
					// 	'message'=>$sms,
					// );

					$checkProfile = $this->rawSelect("select phone_no,user_id from `user` where phone_no='$mobile' limit 1");
					$checkProfile=$checkProfile['0'];

					if (!$checkProfile) {

						$profile_id = $this->rawInsert(
								"insert into user (phone_no,password,account,created_at) "
								. " values('$mobile','$password',1,now())"
								);
						$profile_balance_id = $this->rawInsert(
								"insert ignore into profile_balance (user_id, balance,created_at)"
								. "values ( '$profile_id', 0.0, now()) "
								);

						// $send = $this->sendSMS($sms);

						$this->flashSession->error($this->flashError('Welcome to Betika Tips. Your account has been created. You can login.'));
						$this->response->redirect('login');

						//Disable the view to avoid rendering

						$this->view->disable();

					}else{

						$this->flashSession->error($this->flashError('You are already registered. Use your phone number as your password to login'));
						$this->response->redirect('signup');

						// Disable the view to avoid rendering
						$this->view->disable();
					}
				}
			}
		}
	}

}

