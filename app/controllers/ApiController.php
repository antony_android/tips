<?php

use Phalcon\Session\Bag as SessionBag;
use Phalcon\Http\Response;

use Carbon\Carbon;

class ApiController extends ControllerBase
{

    public function sgettokenAction()
	{
        $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
  
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode('oArARs8yYtnPSWiIHfmPVVdL5gUGCIRg:pSlTUhK4jgKiq3FU');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        
        $curl_response = curl_exec($curl);
        
        echo json_decode($curl_response);
    }


    private static function gettokenAction()
    {
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode('8AkWLty35r6FKf39he0dYbzWlfKIrGGY:yOJ9pzX1DJ7WusWt');
	#$credentials = base64_encode('i7wlE1ExAWS142UKThM2AdA1K1SBmq5k:TQvWO4xsgrWlESl9');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $curl_response = curl_exec($curl);
        
        $response = json_decode($curl_response);
	return $response->access_token;
    }


    private static function paybilltokenAction()
    {
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode('SVH66qBAtH6bKGibhZAWIBjxttt6MGTw:QCoK5cpdAPzsf7Bi');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $curl_response = curl_exec($curl);
        
        $response = json_decode($curl_response);
	    return $response->access_token;
    }


    private static function lipaNaMpesaPassword($lipa_time)
    {
        $passkey = "85a8e678bc84ef1dfba4cbd203299982bc340000b5708eab35a0b38fe49911cf";
        $BusinessShortCode = 731351;
        $timestamp =$lipa_time;
        $lipa_na_mpesa_password = base64_encode($BusinessShortCode.$passkey.$timestamp);
        return $lipa_na_mpesa_password;
    }

    private static function paybillNaMpesaPassword($lipa_time)
    {
        $passkey = "bc9a331469b31711b216c1238810b8e0489c8f27280f74310f080aa19530c2d8";
        $BusinessShortCode = 731029;
        $timestamp =$lipa_time;
        $lipa_na_mpesa_password = base64_encode($BusinessShortCode.$passkey.$timestamp);
        return $lipa_na_mpesa_password;
    }

    public function lipapasswordAction()
    {
        $lipa_time = Carbon::rawParse('now')->format('YmdHms');
        $passkey = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
        $BusinessShortCode = 174379;
        $timestamp =$lipa_time;
        $lipa_na_mpesa_password = base64_encode($BusinessShortCode.$passkey.$timestamp);
        echo "Timestamp ".$timestamp."<br/>";
        echo "Password ".$lipa_na_mpesa_password;
    }


    public function testtimeAction(){

        $lipa_time = Carbon::rawParse('now')->format('YmdHis');
        echo $lipa_time;
    }
    

    public static function stkAction($stake, $msisdn){

        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        $access_token = ApiController::gettokenAction();
  
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$access_token)); //setting custom header
        
        $lipa_time = Carbon::rawParse('now')->format('YmdHis');

        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'BusinessShortCode' => '731351',
            'Password' => ApiController::lipaNaMpesaPassword($lipa_time),
            'Timestamp' => $lipa_time,
            'TransactionType' => 'CustomerBuyGoodsOnline',
            'Amount' => $stake,
            'PartyA' => $msisdn,
            'PartyB' => '731356',
            'PhoneNumber' => $msisdn,
            'CallBackURL' => 'https://betika.tips/api/callback',
            'AccountReference' => 'BTTIPS',
            'TransactionDesc' => 'Account topup'
        );

        $data_string = json_encode($curl_post_data);
  
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        
        $curl_response = curl_exec($curl);
        
        return $curl_response;
    }

    public static function paybillstkAction($stake, $msisdn){

        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        $access_token = ApiController::paybilltokenAction();
  
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$access_token)); //setting custom header
        
        $lipa_time = Carbon::rawParse('now')->format('YmdHis');

        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'BusinessShortCode' => '731029',
            'Password' => ApiController::paybillNaMpesaPassword($lipa_time),
            'Timestamp' => $lipa_time,
            'TransactionType' => 'CustomerPayBillOnline',
            'Amount' => $stake,
            'PartyA' => $msisdn,
            'PartyB' => '731029',
            'PhoneNumber' => $msisdn,
            'CallBackURL' => 'https://betika.tips/api/callback',
            'AccountReference' => 'BTTIPS',
            'TransactionDesc' => 'Account topup'
        );

        $data_string = json_encode($curl_post_data);
  
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        
        $curl_response = curl_exec($curl);
        
        return $curl_response;
    }

    public function callbackAction(){

        $callbackData = file_get_contents('php://input');
        $fp = fopen('/var/log/bt-tips/info.log', 'a');
        fwrite($fp, $callbackData);
        fclose($fp);

        $response = ["status" => 200, "ResultDesc" => "Success"];
        echo json_encode($response);
    }


    public function confirmAction(){

        $callbackData = file_get_contents('php://input');
        // $this->logger($callbackData);

        $payload = json_decode($callbackData);

        $firstname = $payload->FirstName;
        $middlename = $payload->MiddleName;
        $lastname = $payload->LastName;
        $amount = $payload->TransAmount;
        $org_balance = $payload->OrgAccountBalance;
        $msisdn = $payload->MSISDN;
        $transaction_id = $payload->TransID;
        $transaction_type = $payload->TransactionType;
        $transaction_time = $payload->TransTime;
        $short_code = $payload->BusinessShortCode;
        $bill_ref_no = $payload->BillRefNumber;
        $invoice_number = $payload->InvoiceNumber;
        $third_party_trans_id = $payload->ThirdPartyTransID;

        $name = $firstname." ".$middlename." ".$lastname;

        $mpesa_transaction_id = $this->rawInsert(
            "INSERT INTO mpesa_transaction (mpesa_transaction_id, short_code, customer_name,amount,org_balance,
             msisdn, transaction_id, transaction_type, transaction_time, bill_ref_no, invoice_number,
              third_party_trans_id, created_at, updated_at) VALUES(null, '$short_code', '$name','$amount',
              '$org_balance', '$msisdn','$transaction_id', '$transaction_type', '$transaction_time', 
              '$bill_ref_no', '$invoice_number', '$third_party_trans_id', now(), now())"
        );

        $checkProfile = $this->rawSelect("select * from `user` where phone_no='$msisdn' limit 1");

        if (!$checkProfile) {

            $password = $this->security->hash($msisdn);
            $profile_id = $this->rawInsert(
                    "insert into user (phone_no,password,created_at) "
                    . " values('$msisdn','$password',now())"
            );
            
            // if ($this->session->has("betslip")) {

            //     $bets = $this->session->get('betslip');
            //     $count = sizeof($bets);
            //     $amount = $amount - $count;
            // }

            $profile_balance_id = $this->rawInsert(
                    "insert ignore into profile_balance (user_id, balance,created_at)"
                    . "values ( '$profile_id', 0.0, now()) "
            );
            
            $checkProfile = $this->rawSelect("select * from `user` where phone_no='$msisdn' limit 1");
        }

        $checkProfile=$checkProfile['0'];
        
        $user_id = $checkProfile['user_id'];
        $package_id = 2;

        $payment_id = $this->rawInsert(
            "insert into payment (payment_id,user_id,amount,package_id,mpesa_transaction_id, 
             created_at, updated_at)  values(null, '$user_id','$amount','$package_id',
             '$mpesa_transaction_id',now(),now())"
        );

        $this->rawInsert(
            "update profile_balance set balance = balance+$amount where user_id = '$user_id'"
        );

        $limit = intval($amount);

        $query = "SELECT * FROM `user_tip` ut INNER JOIN `user_tipslip` us 
        ON ut.user_tip_id = us.user_tip_id INNER JOIN `tip` t ON us.game_id = t.game_id WHERE 
        ut.user_id='$user_id' AND us.prediction IS NULL AND t.start_time > NOW() ORDER BY 1 DESC
         LIMIT $limit";

        $fp = fopen('/var/log/bt-tips/info.log', 'a');
        fwrite($fp, $query);
        fclose($fp);

        $checkUnpaidTips = $this->rawSelect($query);

        $smsMessage = "";

        if ($checkUnpaidTips) {
            
            foreach ($checkUnpaidTips as $match) {

                $game_id = $match['game_id'];
                $user_tipid = $match['user_tip_id'];
                $home_team = $match['home_team'];
				$away_team = $match['away_team'];
                $start_time = $match['start_time'];
                $subs = explode(" ", $start_time);
                $kick_off = substr($subs[1], 0, 5);

                $checkTips = $this->rawSelect("SELECT * from `tip` where game_id='$game_id' limit 1");
                $checkTips = $checkTips['0'];
                $pred = $checkTips['prediction'];

                $this->rawInsert(
                    "update user_tipslip set prediction='$pred', status='2' where user_tip_id='$user_tipid'"
                );

                $smsMessage = $smsMessage."ID ".$game_id." TIME: ".$kick_off." ".strtoupper(substr($home_team, 0, 6))." vs ".strtoupper(substr($away_team, 0, 6))." - ".strtoupper($pred)." ";
            }

            $fp = fopen('/var/log/bt-tips/info.log', 'a');
            fwrite($fp, $smsMessage);
            fclose($fp);

            $smsMessage = $smsMessage." betika.tips TILL 731356";

            $this->rawInsert(
                "insert into outbox (id,msisdn,message,status,created_at, updated_at)
                  values(null, '$msisdn','$smsMessage','0',now(),now())"
            );

            $this->rawInsert(
                "update profile_balance set balance = balance-$amount where user_id = '$user_id'"
            );

            $feedback = "Your tips have been saved. Please go to \"My Tips\" page to view your tips.
                     Thank you.";
            $this->flashSession->success($this->flashSuccess($feedback));
        }
    }

    public function validateAction(){

        $response = ["status" => 200, "message" => "Success"];
        echo json_encode($response);
    }

}

?>