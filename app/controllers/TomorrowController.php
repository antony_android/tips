<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class TomorrowController extends ControllerBase
{
	public function initialize()
    {
        $this->tag->setTitle('About us');
    }

    public function indexAction()
    {
        $page = $this->request->get('page','int') ?: 0;
        if ($page<0) {
            $page=0;
        }
        $sport_id = $this->request->get('id','int') ?: 79;
        $limit = $this->request->get('limit','int') ?: 100;
        $skip = $page*$limit;

        $keyword = $this->request->getPost('keyword');
        $cache_key = "tomorrow.controller" . $sport_id . preg_replace("/\s+/","-", $keyword) .$skip . $limit;
            
	    $cached_games = $this->redisCache->get($cache_key);
        if(!empty($cached_games)){
            $today = $cached_games[0];
            $total = $cached_games[1];
            $current_sport = $cached_games[2]; 
        } else{
			$current_sport = [];
			foreach($this->sports as $key => $sp){
				if($sp['sport_id'] == $sport_id){
					$current_sport = $sp;
					break;
				}
			}
			$default_sub_type_id = $current_sport['default_market'];

            list($today, $total) = $this->getGames(
                $keyword, $skip, $limit, $sport_id, 
				'and date(start_time) = curdate() + INTERVAL 1 DAY', 
				' m_priority desc, priority desc, start_time asc',
				$default_sub_type_id
			);

            $min_time = $this->getMinTime($keyword, $skip, $limit, $sport_id, "",
				" m_priority desc, priority desc,  start_time asc ", 
			    $default_sub_type_id
			);
            $lifetime = $min_time - time();
            $this->redisCache->set($cache_key, [$today, $total, $current_sport], $lifetime);

        }

        $total = $total['0']['total'];

        $pages = ceil($total/$limit)-1;

        if ($pages>12) {
            $pages = 12;
        }

        $tab = 'tomorrow';

        $this->view->setVars(
            ['matches'=>$today,
            'total'=>$total,
            'tab'=>$tab,
            'pages'=>$pages,
            'page'=>$page,
            'current_sport' => $current_sport,
            ]
        );

        $this->tag->setTitle('Surebet');

        $this->view->pick('index/index');
    }

}

