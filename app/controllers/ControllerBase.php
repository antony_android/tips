<?php

use Firebase\JWT\JWT;
use Phalcon\Mvc\Controller;
/**
 * Class ControllerBase
 */
class ControllerBase extends Controller
{
    /**
     *
     */
    const JWT_KEY = "2bdVweTeI42s5mkLdYHyklTMxQS5gLA7MDS6FA9cs1uobDXeruACDic0YSU3si04JGZe4Y";
    /**
     *
     */
    const BASE_URI = "https://betika.tips:8080";

    /**
     * @param $dispatcher
     */
    public function beforeExecuteRoute($dispatcher)
    {
        if ($this->cookies->has('auth')) {
            $token = $this->cookies->get('auth');
            if (!$this->session->has("auth")) {
                try {
                    $user = JWT::decode($token, self::JWT_KEY, ['HS256']);
                    $user = $user->user;
                    if ($user->remember == '1' || $user->device == '1') {
                        $user = [
                            'id'       => $user->id,
                            'name'     => $user->name,
                            'mobile'   => $user->mobile,
                            'device'   => $user->device,
                            'remember' => $user->remember,
                        ];
                        $this->_registerSession($user);
                    }

                } catch (Exception $e) {
                    $decoded = $e->getMessage();
                }

            }
        }

        $betslip = $this->betslip('betslip');
         
        $slipCount = sizeof($betslip);
        // $this->sports = $this->get_sports_via_cache();
        $refURL = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        // $this->free_bet = $this->get_free_bet();

        $this->view->setVars([
            'slipCount' => $slipCount,
            'refURL'    => $refURL,
            'betslip'   => $betslip,
            'betslip_data' => $this->betslip_meta(),
			// 'sports' => $this->sports,
            // 'freebet' => $this->free_bet,
            'current_sport' => ['sport_id'=>79, 'sport_name' =>'Soccer'],
        ]);
    }

    // private function get_free_bet(){

    //     $free_bet_game = $this->redisCache->get('free_bet_game');
	// 	if(empty($free_bet_game)){
	// 		$sql = "select m.home_team, m.away_team, o.parent_match_id, o.sub_type_id,"
	// 			. " group_concat(o.odd_value order by field(o.odd_key, m.home_team, 'draw', m.away_team)) as odds, "
	// 			. " m.game_id, m.start_time from free_bet_game fg "
	// 			. " inner join `match` m on m.parent_match_id = fg.parent_match_id  "
	// 			. " inner join event_odd o on (fg.parent_match_id = o.parent_match_id "
	// 			. " and  fg.sub_type_id = o.sub_type_id)  where  m.start_time > now() "
	// 			. " group by parent_match_id order by start_time asc limit 1;";
	// 		 $freebet_game = $this->rawSelect($sql);
    //          if(!empty($freebet_game)) {
	// 	         $t_game = strtotime($freebet_game[0]['start_time']);
	// 	     }else{
    //              return null;
    //          }
    //          $t_fifteen_min = time()+600;
	// 	     $rv = $t_game > $t_fifteen_min ? $t_fifteen_min : $t_game;
    //          $cache_time = $rv - time();
    //          $this->redisCache->set('freebet_game', $freebet_game, $cache_time);
    //      }
	// 	 return $freebet_game[0];

    // }
    
    // private function get_sports_via_cache(){

    //    $sports = $this->redisCache->get('sports');
    //     if(empty($sports)){
    //         $sports = $this->rawSelect(
    //             "select sport.*, (select count(*) from competition c "
    //             . " inner join `match` m on m.competition_id= c.competition_id "
    //             . " where sport_id = sport.sport_id and m.start_time > now())cc "
    //             . " from sport having cc> 0;"
    //         );
    //         $this->redisCache->set('sports', $sports, 7200);
    //     }
    //     return $sports;

    // }

    /**
     * @param $user
     */
    protected function registerAuth($user)
    {
        $exp = time() + (3600 * 24 * 5);
        $token = $this->generateToken($user, $exp);
        $this->cookies->set('auth', $token, $exp);
        $this->_registerSession($user);
    }
    /**
     * @param null $bet_type
     *
     * @return array
     */
    protected function betslip_meta($bet_type = null)
    {
        $betslip_data = $this->session->get("betslip_data") ?: [];
        if(empty($betslip_data)){
            $betslip_data['bet_amount'] = 49;
            $betslip_data['total_odd'] = 1;
            $betslip_data['possible_win'] = 49;
        } 
        return $betslip_data;
    }


    /**
     * @param null $bet_type
     *
     * @return array
     */
    protected function betslip($bet_type = null)
    {
        $betslip = $this->session->get("betslip") ?: [];
        return $betslip;
    }

    /**
     * @param $bet_type
     *
     * @return bool
     */
    protected function betslipUnset($bet_type)
    {
        $betslip = $this->session->get("betslip");

        foreach ($betslip as $key => $value) {
            if ($value['bet_type'] == $bet_type) {
                unset($betslip[$key]);
            }
        }

        $this->session->set("betslip", $betslip);

        return true;
    }

    /**
     * @param $user
     */
    private function _registerSession($user)
    {
        $this->session->set('auth', $user);
    }

    /**
     * @param $data
     * @param $exp
     *
     * @return string
     */
    protected function generateToken($data, $exp)
    {
        $token = [
            "iss"  => "https://betika.tips",
            "iat"  => 1356999524,
            "nbf"  => 1357000000,
            "exp"  => $exp,
            "user" => $data,
        ];

        $jwt = JWT::encode($token, self::JWT_KEY);

        return $jwt;
    }

    /**
     * @param $token
     *
     * @return object
     */
    protected function decodeToken($token)
    {
        $decoded = JWT::decode($token, self::JWT_KEY, ['HS256']);

        return $decoded;
    }

    /**
     * @param $statement
     *
     * @param array $bindings
     *
     * @return mixed
     */
    protected function rawSelect($statement, $bindings = [])
    {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement, $bindings);
        $success->setFetchMode(Phalcon\Db\Enum::FETCH_ASSOC);
        $success = $success->fetchAll($success);

        return $success;
    }

    /**
     * @param $statement
     *
     * @return mixed
     */
    protected function rawQueries($statement)
    {
        return $this->rawSelect($statement);
    }

    /**
     * @param $statement
     *
     * @return mixed
     */
    protected function rawInsert($statement)
    {
        $connection = $this->di->getShared("db");
        $success = $connection->query($statement);

        $id = $connection->lastInsertId();

        return $id;
    }

    /**
     * @param $message
     *
     * @return string
     */
    protected function flashError($message)
    {
//        return "<div class='alert alert-error alert-dismissible'>$message</div>";
        return $message;
    }

    /**
     * @param $message
     *
     * @return string
     */
    protected function flashSuccess($message)
    {
//        return "<div class='alert alert-success alert-dismissible' role='alert'>$message</div>";
        return $message;
    }

    /**
     * @param $number
     *
     * @return bool|string
     */
    protected function formatMobileNumber($number)
    {
        $regex = '/^(?:\+?(?:[1-9]{3})|0|)?([712][0-9]{8})$/';
        if (preg_match($regex, $number, $capture)) {
            $msisdn = '254' . $capture[1];
        } else {
            $msisdn = false;
        }

        return $msisdn;
    }

    /**
     * @param $url
     * @param $data
     *
     * @return mixed
     */
    protected function getData($url, $data)
    {
        $httpRequest = curl_init($url);
        curl_setopt($httpRequest, CURLOPT_URL, $url);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ',
        ]);
        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);
        //decode response
        $response = json_decode($results);

        return $response;
    }

    /**
     * @return string
     */
    protected function getDevice()
    {
        $device = '2';
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $device = '1';
        }

        return $device;
    }

    /**
     * @return array|false|string
     */
    function getClientIP()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    /**
     * @param $data
     * @param $url
     *
     * @return array
     */
    protected function betJackpot($data, $url)
    {
        $bet = json_encode($data);

        $httpRequest = curl_init($url);

        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($bet),
        ]);
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $results,
        ];

        return $response;
    }

    /**
     * @param $transaction
     *
     * @return array
     */
    protected function betTransaction($transaction)
    {
        $URL = self::BASE_URI . "/bet";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $results,
        ];

        return $response;
    }

    /**
     * @param $sms
     *
     * @return mixed
     */
    protected function sendSMSRubeApi($sms)
    {

        $URL = self::BASE_URI . "/sendsms";
	    $sms_message = http_build_query($sms + [ 'link_id' => '']);

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $sms_message);
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }


    protected function sendSMS($sms)
    {

        $msisdn = $sms["msisdn"];
        $smsMessage = $sms["message"];
        
        $this->rawInsert(
            "insert into outbox (id,msisdn,message,status,created_at, updated_at)
              values(null, '$msisdn','$smsMessage','0',now(),now())"
        );

    }

     /**
     * @param $data
     *
     * @return array
     */
    protected function free_bet($data)
    {
        $URL = self::BASE_URI . "/free-bet";

        $bet = json_encode($data);

        $httpRequest = curl_init($URL);

        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($bet),
        ]);
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $results,
        ];

        return $response;
    }

    /**
     * @param $data
     *
     * @return array
     */
    protected function bet($data)
    {
        $URL = self::BASE_URI . "/bet";

        $bet = json_encode($data);

        $httpRequest = curl_init($URL);

        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $bet);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($bet),
        ]);
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $results,
        ];

        return $response;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    protected function topup($data)
    {

        $URL = self::BASE_URI . "/stk/depost";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$data");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }

    /**
     * @param $transaction
     *
     * @return mixed
     */
    protected function withdraw($transaction)
    {

        $URL = self::BASE_URI . "/withdraw";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return ['status_code' => $status_code, 'response' => $response];
    }

    /**
     * @param $transaction
     *
     * @return mixed
     */
    protected function bonus($transaction)
    {

        $URL = self::BASE_URI . "/profilemgt";

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, "$transaction");
        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = json_decode($results);

        return $status_code;
    }


   /**
     * @param $transaction
     *
     * @return array
     */
    protected function payBet($betData)
    {
        $URL = self.BASE_URI . "/paybet";
        $betData =  json_encode($betData);

        $httpRequest = curl_init($URL);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS,$betData);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($betData),
        ]);

        curl_setopt($httpRequest, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)');

        $results = curl_exec($httpRequest);
        $status_code = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
        curl_close($httpRequest);

        $response = [
            "status_code" => $status_code,
            "message"     => $results,
        ];

        return $response;
    }

    /**
     * @param $keyword
     * @param $skip
     * @param $limit
     * @param string $filter
     * @param string $orderBy
     *
     * @return array
     */
    protected function getMinTime($keyword, $skip, $limit=1, $sport_id=79, 
        $filter = "", $orderBy = " m_priority desc, priority desc,  start_time asc", $sub_type_id=1)
    {
        $where = "where sport_id = :sport_id  and sub_type_id = :sub_type_id and start_time > now() ";
        $where .= $filter;

        $bindings = ['sport_id' => $sport_id, 'sub_type_id' => $sub_type_id];
        if (!empty($keyword)) {
            $bindings['keyword'] = "%$keyword%";
            $whereClause = "$where and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword)";
            $game = $this->rawSelect("select min(start_time) as start_time from (select start_time from ux_todays_highlights $whereClause order by $orderBy limit $skip,$limit)c", $bindings);
        } else {
            $query = "select min(start_time) as start_time from (select start_time from ux_todays_highlights " . $where . " order by $orderBy limit $skip,$limit)c" ;

       // die($query);
            $game = $this->rawSelect($query, $bindings);
        }
		$t_fifteen_min = time()+600;
		if(!empty($game)) {
		   $t_game = strtotime($game[0]['start_time']);
		}
		$rv = $t_game > $t_fifteen_min ? $t_fifteen_min : $t_game;
		return $rv;
		/*
			return [
				$rv,
				$query
			];
		*/
    }

    /**
     * @param $keyword
     * @param $skip
     * @param $limit
     * @param string $filter
     * @param string $orderBy
     *
     * @return array
     */
    protected function getGames($keyword,   $filter = "", $orderBy = "start_time asc")
    {
        $where = "where start_time > now() ";
        $where .= $filter;

        $bindings = [];
        if ($keyword) {
            $bindings['keyword'] = "%$keyword%";
            $whereClause = "$where and (game_id like :keyword or home_team like :keyword or away_team like :keyword or competition_name like :keyword)";
            $matches = $this->rawSelect("select * from tip $whereClause  AND prediction IS NOT NULL  order by $orderBy", $bindings);
            $items = $this->rawSelect("select count(*) as total from tip $whereClause  AND prediction IS NOT NULL limit 1", $bindings);
        } else {
            $query = "select * from tip " . $where . " AND prediction IS NOT NULL order by $orderBy" ;
            $matches = $this->rawSelect($query, $bindings);
            $items = $this->rawSelect("select count(*) as total from tip $where  AND prediction IS NOT NULL limit 1", $bindings);
        }

        return [
            $matches,
            $items,
        ];
    }



       
    /**
     * @param $message
     *
     * @return mixed
     */
    protected function formatMessage($message)
    {
        return preg_replace('/(KES\s+)?[+-]?[0-9]{1,3}(?:,?[0-9]{3})(\.[0-9]{2})?/', '<b>$0</b>', $message);
    }

    /**
     * @param $keyword
     * @param $skip
     * @param $limit
     *
     * @param string $filter
     * @param string $orderBy
     *
     * @return array
     */
    protected function getLiveGames($keyword, $skip, $limit, $filter = "",
        $orderBy = "m.priority desc, c.priority desc, m.start_time asc")
    {
        $where = "where m.status <> 3 AND m.start_time > now()-interval 4 hour AND o.sub_type_id =1 and m.active=1 " . $filter;

        //$where = "where m.status <> 3  AND o.sub_type_id =1 " . $filter;

       
        $whereClause = "$where ";


        $query ="SELECT c.priority, '' AS side_bets, o.sub_type_id, 
            group_concat(if(o.odd_key = m.home_team, o.odd_value, null)) as home_odd, 
            group_concat(if(o.odd_key = 'draw', o.odd_value, null)) as neutral_odd, 
            group_concat(if(o.odd_key = m.away_team, o.odd_value, null)) as away_odd,  
            group_concat(if(o.odd_key = m.home_team, o.active, null)) as home_odd_status, 
            group_concat(if(o.odd_key = 'draw', o.active, null)) as draw_odd_status, 
            group_concat(if(o.odd_key = m.away_team, o.active, null)) as away_odd_status, 
            o.market_active, m.match_id, m.start_time, m.away_team, m.home_team,
            m.parent_match_id, c.competition_name,c.category, m.match_time, 
            m.score, m.bet_status, m.match_status, m.event_status, m.home_red_card, 
            m.away_red_card, m.home_yellow_card, m.away_yellow_card, m.home_corner, 
            m.away_corner, m.active as event_live FROM `live_odds_change` o inner join
            live_match m on o.parent_match_id=m.parent_match_id INNER JOIN competition c 
            ON c.competition_id = m.competition_id INNER JOIN sport s 
            ON s.sport_id = c.sport_id where s.sport_id=79 AND 
            o.sub_type_id =1 and m.event_status='Live' and m.active=1 GROUP BY m.parent_match_id,
            o.sub_type_id,o.special_bet_value ORDER BY $orderBy LIMIT 0,200";

        //die($query);

        $today = $this->rawQueries($query);

        $results = [];


        foreach ($today as $day) {
            $results[(new DateTime($day['start_time']))->format($this->getDefaultDateFormat())][] = $day;
        }

        return [
            $results,
            $items,
        ];
    }

    protected function getResultPages($total, $limit)
    {
        $pages = ceil($total / $limit);

        if ($pages > 14) {
            $pages = 14;
        }

        return $pages;
    }
    
    protected function getPaginationParams()
    {
        $page = $this->request->get('page', 'int') ?: 0;
        if ($page < 0) {
            $page = 0;
        }
        $limit = $this->request->get('limit', 'int') ?: 100;
        $skip = $page * $limit;

        return [
            $page,
            $limit,
            $skip,
        ];
    }

}
