<?php

class DepositController extends ControllerBase
{
    public function IndexAction() {
      
    }

    public function topupAction()
    {
        $amount = $this->request->getPost('amount', 'int');
        $msisdn = $this->request->getPost('msisdn', 'int');
        
        if ($amount<5) {
            $this->flashSession->error($this->flashError('Sorry, minimum top up amount is Ksh. 5'));
            return $this->response->redirect('deposit');
            $this->view->disable();
        }elseif ($amount>150000) {
            $this->flashSession->error($this->flashError('Sorry, maximum top up amount is KES. 150,000'));
            return $this->response->redirect('deposit');
            $this->view->disable();
        }else{

            echo ApiController::stkAction($amount, $this->formatMobileNumber($msisdn));

            $this->flashSession->error($this->flashSuccess('Lipa na M-PESA Online transaction has been initiated on your phone, your account will credited immediately on successful completion of the transaction.'));
    
            $this->response->redirect('deposit');
            // Disable the view to avoid rendering
            $this->view->disable();
        }
    }
    
}

?>
