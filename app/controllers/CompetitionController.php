<?php

/**
 * Created by PhpStorm.
 * User: mxgel
 * Date: 03/02/2018
 * Time: 15:32
 */
class CompetitionController extends ControllerBase
{

    /**
     *
     */
    public function indexAction()
    {
        $id = $this->request->get('id', 'int');
         
        $cache_key = 'competiton'.$id;
        list($theCompetition, $matches, $current_sport) = $this->redisCache->get($cache_key);

        if(empty($theCompetition)){
			$theCompetition = $this->rawQueries(
				"select competition_name,competition_id,category, sport_name, "
				. " sport.sport_id from competition inner join sport using(sport_id) "
				. " where competition_id='$id' limit 1"
			);
			$sport_id = $theCompetition[0]['sport_id'];
			$current_sport = [];
			foreach($this->sports as $key => $sp){
				if($sp['sport_id'] == $sport_id){
					$current_sport = $sp;
					break;
				}
			}
			$default_market = $current_sport['default_market'];

			$matches = $this->rawQueries(
				"select c.priority, (select count(distinct e.sub_type_id) "
				. " from event_odd e inner join odd_type o on "
				. " (o.sub_type_id = e.sub_type_id and o.parent_match_id = e.parent_match_id "
				. " and e.sub_type_id not in (66,65,16)) where e.parent_match_id = m.parent_match_id "
				. " and o.active = 1) as side_bets, o.sub_type_id, "
				. " MAX(CASE WHEN o.odd_key = m.home_team THEN odd_value END) AS home_odd, "
				. " MAX(CASE WHEN o.odd_key = 'draw' THEN odd_value END) AS neutral_odd, "
				. " MAX(CASE WHEN o.odd_key = m.away_team THEN odd_value END) AS away_odd, "
				. " m.game_id, m.match_id, m.start_time, m.away_team, m.home_team, "
				. " m.parent_match_id,c.competition_name,c.category from `match` m "
				. " inner join event_odd o on m.parent_match_id = o.parent_match_id "
				. " inner join competition c on c.competition_id = m.competition_id "
				. " inner join sport s on s.sport_id = c.sport_id "
				. " where c.competition_id='$id' and m.start_time > now() "
				. " and o.sub_type_id = '$default_market'"
				. " and m.status <> 3 group by m.parent_match_id "
				. " order by m.priority desc, c.priority desc , m.start_time limit 200"
			);

            $matchTmeResult =  $this->rawQueries(
			    "select min(m.start_time) as start_time from `match` m "
				. " inner join event_odd o on m.parent_match_id = o.parent_match_id "
				. " inner join competition c on c.competition_id = m.competition_id "
				. " inner join sport s on s.sport_id = c.sport_id "
				. " where c.competition_id='$id' and m.start_time > now() and o.sub_type_id = '$default_market'"
				. " and m.status <> 3 "
			);
			$max_cache_time = time()+600;
            $match_time = strtotime($matchTimeResult['start_time']);
            $lifetime = ($match_time > $max_cache_time ? $max_cache_time : $match_time) - time();
            $this->redisCache->set($cache_key, [$matchInfo, $subTypes], $lifetime);

        }

        $title = $theCompetition['0']['sport_name']. " - " . $theCompetition['0']['competition_name'] . ", " . $theCompetition['0']['category'];

        $this->tag->setTitle($title);

        $this->view->setVars([
            'matches'        => $matches,
            'title'          => $title,
            'current_sport' => $current_sport,
        ]);
        $this->view->pick("sports/threeway");
    }
}
