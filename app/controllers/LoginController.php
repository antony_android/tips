<?php

/**
 * Class LoginController
 */
class LoginController extends ControllerBase
{
    /**
     *
     */
    public function IndexAction()
    {
        $ref = $this->request->get('ref') ?: '';
        $this->view->setVars(['ref' => $ref]);
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function authenticateAction()
    {
        if ($this->request->isPost()) {
            $mobile = $this->request->getPost('mobile', 'int');
            $remember = $this->request->getPost('remember', 'int') ?: 0;
            $password = $this->request->getPost('password');
            $refURL = $this->request->getPost('ref') ?: '';
            $refU = "login?ref=" . $refURL;

            if (!$mobile || !$password || !preg_match('/^(?:\+?(?:[1-9]{3})|0)?([7,1]([0-9]{8}))$/', $mobile)) {
                $this->flashSession->error($this->flashError('All fields are required'));

                return $this->response->redirect($refU);
                $this->view->disable();
            }

            $mobile = $this->formatMobileNumber($mobile);

            if ($mobile) {
                $user = $this->rawSelect("SELECT * from user where phone_no='$mobile' limit 1");
                $user = $user['0'];

                if ($user == false) {
                    $this->flashSession->error($this->flashError('Invalid username/password'));
                    $this->response->redirect($refU);
                    // Disable the view to avoid rendering
                    $this->view->disable();
					return;
                }

                $thePassword = $user['password'];

                if (!$this->security->checkHash($password, $thePassword)) {
                    $this->flashSession->error($this->flashError('Invalid username/password'));
                    $this->response->redirect($refU);
                    // Disable the view to avoid rendering
                    $this->view->disable();
                } else {
                    $device = $this->getDevice();
                    $sessionData = ['id'       => $user['user_id'],
                                    'remember' => $remember,
                                    'mobile'   => $mobile,
                                    'device'   => $device
                    ];
                    $exp = time() + (3600 * 24 * 5);
                    $this->registerAuth($sessionData, $exp);
                    $this->response->redirect($refURL);
                }

            }

        }
    }

}

?>
