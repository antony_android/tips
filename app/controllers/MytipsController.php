<?php
/**
 * Copyright (c) Antony 2020.
 *
 * All rights reserved.
 */

class MytipsController extends ControllerBase
{

	public function indexAction()
	{
		$tipID = $this->request->get('id','int');
        $a = $this->request->get('a','int');

		if(!is_numeric($tipID))
			$tipID = 0;

		$id = $this->session->get('auth')['id'];

		if ($id && $a!=1) {

			$myTips = $this->rawQueries("select b.user_tip_id,b.created_at, b.tip_message, b.tip_amount,
			b.status, s.prediction, if(FIND_IN_SET(b.status,(select group_concat(status) from user_tipslip
			 where user_tip_id = b.user_tip_id)), b.status, 1) xstatus from user_tip b inner join user_tipslip 
			s on b.user_tip_id=s.user_tip_id where b.status != 1 and b.user_id=$id group by s.user_tip_id order by 
			b.created_at desc limit 30;");

			$title = "My Tip Slips";

			$this->tag->setTitle($title);

			$this->view->setVars(["myTips"=>$myTips]);

			if ($tipID != 0) {
				$myTip = $this->rawQueries("select b.user_tip_id,b.created_at, b.bet_message,b.tip_amount,
				b.status, s.prediction, b.user_id, if(FIND_IN_SET(b.status,(select group_concat(status)
				 from user_tipslip where user_tip_id = b.user_tip_id)), b.status, 1) xstatus from user_tip b
				  inner join user_tipslip s on b.user_tip_id=s.user_tip_id where b.status != 1 and 
				  b.user_tip_id='$tipID' group by s.user_tip_id order by b.created_at desc limit 40");

				$myTip = $myTip['0'];
				$tipDetails = $this->rawQueries("select b.user_tip_id, b.created_at, t.start_time, b.tip_amount,
				 b.status, s.prediction, t.game_id, t.away_team, t.result, t.home_team, s.odd_value from user_tip
				  b inner join user_tipslip s on s.user_tip_id = b.user_tip_id inner join `tip` t on t.game_id
				   = s.game_id where s.user_tip_id = '$tipID' group by s.user_tipslip_id order by b.user_tip_id
				    desc");

				$this->view->setVars(["tipDetails"=>$tipDetails,'myTip'=>$myTip]);
				$this->view->pick("mytips/details");

			}

		}else{
			if(!$tipID){

				$this->flashSession->error($this->flashSuccess('Login to access this page'));
				$this->response->redirect('login');
			}else{

				$tipDetails = $this->rawQueries("select b.user_tip_id, b.created_at, t.start_time, b.tip_amount,
				 b.status, s.prediction, t.game_id, t.away_team, t.result, t.home_team, s.odd_value from user_tip
				  b inner join user_tipslip s on s.user_tip_id = b.user_tip_id inner join `tip` t on t.game_id
				   = s.game_id where s.user_tip_id = '$tipID' group by s.user_tipslip_id order by b.user_tip_id
				    desc");

				$myTip = $tipDetails['0'];

				$this->view->setVars(["tipDetails"=>$tipDetails, 'myTip'=>$myTip]);

				$this->view->pick("mytips/tip-details");
			}
		}
	}


	public function viewAction()
	{
		$tipID = $this->request->get('id','int');

		if(!is_numeric($tipID))
			$tipID = 0;
		if(!$tipID){
			$this->flashSession->error($this->flashSuccess('Login to access this page'));
			$this->response->redirect('login');
		}else{

			$tipDetails = $this->rawQueries("select b.user_tip_id, b.created_at, t.start_time, b.tip_amount,
			 b.status, s.prediction, t.game_id, t.away_team, t.result, t.home_team, s.odd_value from user_tip
			  b inner join user_tipslip s on s.user_tip_id = b.user_tip_id inner join `tip` t on m.game_id
			   = s.game_id where s.user_tip_id = '$tipID' group by s.user_tipslip_id order by b.user_tip_id
			    desc");

			$myTip = $tipDetails['0'];

			$this->view->setVars(["tipDetails"=>$tipDetails, 'myTip'=>$myTip]);

			$this->view->pick("mytips/tip-details");
		}
 

	}


    public function showAction() {

        $tipID = $this->request->get('id','int');
		$myTip = $this->rawSelect("select b.user_tip_id,b.total_odd,b.created_at,b.tip_message,b.tip_amount,
		 b.status, b.user_id, s.prediction, if(FIND_IN_SET(b.status,(select group_concat(status) from user_tipslip
		  where user_tip_id = b.user_tip_id)), b.status, 1) xstatus from user_tip b inner join user_tipslip s on 
		 b.user_tip_id=s.user_tip_id where b.user_tip_id='$tipID' group by s.user_tip_id order by b.created_at
		  desc limit 1");
		  
		$myTip = $myTip['0'];
		
		$tipDetails = $this->rawSelect("select b.user_tip_id, b.total_odd,b.created_at, t.start_time, b.tip_amount,
		 b.status, t.game_id, t.away_team, t.result, s.prediction, t.home_team, s.odd_value from user_tip b inner
		  join user_tipslip s on s.user_tip_id = b.user_tip_id inner join `tip` t on t.game_id = s.game_id where
		   s.user_tip_id = '$tipID' group by s.user_tipslip_id order by b.user_tip_id desc");

        $this->view->setVars(["tipDetails"=>$tipDetails,'myTip'=>$myTip]);                
    }

}