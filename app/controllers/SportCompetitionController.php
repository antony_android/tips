<?php

/**
 * Class FootballController
 */
class SportCompetitionController extends ControllerBase
{
    /**
     *
     */
    public function IndexAction()
    {
        $id = $this->request->get('id', 'int');
        $id = $id ?: 79;

        list($competitions, $top_competitions) = $this->redisCache->get('sport-competitions-'.$id);
        if(empty($competitions)){
			$competitions = $this->rawSelect(
				"SELECT * FROM ux_categories WHERE sport_id=:id and games_count > 0", ['id'=>$id]
			 );
            $top_competitions = $this->rawSelect(
            "SELECT competition.competition_id, competition.competition_name,"
            . " category.country_code, count(*) AS games_count FROM competition "
            . " INNER JOIN category ON category.category_id = competition.category_id "
            . " INNER JOIN `match` ON `match`.competition_id = competition.competition_id "
            . " WHERE competition.sport_id = :id AND `match`.start_time > now() "
            . " GROUP BY competition.competition_id having games_count > 0 "
            . " ORDER BY competition.priority  DESC LIMIT 10;",
            ['id' => $id]);
 
            $this->redisCache->set('sport-competitions-'.$id, [$competitions, $top_competitions], 7200);
      
        }
       
      
        $this->view->setVars([
            'competitions'     => $competitions,
            'men'          => 'sport-competition',
            'sport_name' => $competitions[0]['sport_name'],
            'top_competitions' => $top_competitions,
        ]);
    }


}
