<?php

class ResetpasswordController extends ControllerBase
{
    public function indexAction()
    {
        $id = $this->request->get('id','int');

        if ($id) {
            $user = $this->rawSelect("SELECT * from user where user_id='$id' limit 1");
            $user = $user['0'];
    
            $this->view->setVars(["msisdn"=>$user['phone_no'],"profile_id"=>$user['user_id']]);
        }
    }

    public function codeAction()
    {
        $verification_code = substr(number_format(time() * mt_rand(),0,'',''),0,4);
        $mobile = $this->request->getPost('mobile', 'int');

        if (!$mobile) {
            $this->flashSession->error($this->flashError('Please enter your mobile number'));
            $this->response->redirect('resetpassword');

            $this->view->disable();
        }else{
            $mobile = $this->formatMobileNumber($mobile);

            if ($mobile==false) {
                $this->flashSession->error($this->flashError('Invalid mobile phone number'));
                $this->response->redirect('resetpassword');

                // Disable the view to avoid rendering
                $this->view->disable();
            }else{

                $user = $this->rawSelect("SELECT * from user where phone_no='$mobile' limit 1");
                $user = $user['0'];
        
                if (!$user) {
                    $this->flashSession->error($this->flashError('We cannot find a user with the phone number provided. Please confirm your mobile number or register to continue'));
                    $this->response->redirect('resetpassword');
                    // Disable the view to avoid rendering
                    $this->view->disable();
                }else{
                    
                    $sms = "Your Betika Tips password reset code is $verification_code. Use this to reset your password.";
                    $sms = ["msisdn" => $mobile, "message" => $sms ];
        
                    $profile_id = $user['user_id'];
                    $this->rawInsert("update user set verification_code='$verification_code' where user_id='$profile_id'");

                    $this->sendSMS($sms);
                    $this->flashSession->error($this->flashSuccess('Use the code sent to your phone to reset your password'));
                    $this->response->redirect("resetpassword?id=$profile_id");
                    $this->view->disable();
                }
            }
        }
    }

    public function passwordAction()
    {
        $password = $this->request->getPost('password');
        $repeatPassword = $this->request->getPost('repeatPassword');
        $reset_code = $this->request->getPost('reset_code','int');
        $profile_id = $this->request->getPost('profile_id','int');

        if (!$password || !$reset_code || !$repeatPassword || !$profile_id) {
            $this->flashSession->error($this->flashError('All fields are required'));
            $this->response->redirect("resetpassword?id=$profile_id");

            // Disable the view to avoid rendering
            $this->view->disable();
        }else{
                if ($password != $repeatPassword) {
                    $this->flashSession->error($this->flashError('Passwords do not match'));
                    $this->response->redirect("resetpassword?id=$profile_id");
        
                    // Disable the view to avoid rendering
                    $this->view->disable();
                }else{
                    $valid = $this->rawSelect("select user_id from user where user_id='$profile_id' and verification_code='$reset_code'");
                    if ($valid) {
                       $password = $this->security->hash($password);
                       $this->rawInsert("update user set password='$password' where user_id='$profile_id'");
                       $this->flashSession->error($this->flashSuccess('Password successfully reset'));
                        $this->response->redirect('login');
        
                        // Disable the view to avoid rendering
                        $this->view->disable();
                    }else{
                        $this->flashSession->error($this->flashError('Invalid reset code'));
                        $this->response->redirect("resetpassword?id=$profile_id");
        
                        // Disable the view to avoid rendering
                        $this->view->disable();
                    }
                }
            }
    }

}

?>
