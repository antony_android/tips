<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Http\Response;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('About us');
    }

    public function indexAction()
    {

        
		$keyword = $this->request->getPost('keyword', 'string');
		
		list($page, $limit, $skip) = $this->getPaginationParams();

        $where = "";

        $orderBy = " priority desc";

        list($matches, $total, $sCompetitions) = $this->getGames($keyword, $where, $orderBy);

        $total = $total['0']['total'];

        $theBetslip = $this->session->get("betslip");

        $this->view->setVars([
            'matches'       => $matches,
            'theBetslip'    => $theBetslip,
            'total'         => $total,
			'pages'         => $this->getResultPages($total, $limit),
			'page'          => $page,
            'keyword'       => $keyword
		]);

        $this->tag->setTitle('Betika Tips');

    }

}