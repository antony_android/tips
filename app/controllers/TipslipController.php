<?php

use Phalcon\Session\Bag as SessionBag;
use Phalcon\Http\Response;

class TipslipController extends ControllerBase
{
	public function indexAction()
	{
		$stake = $this->session->get('stake') ?: 5;
		$betslip = $this->session->get("betslip")?: [];
		$totalOdd = 1;
		$odds = 1;
        array_walk($betslip, function($slip, $mid)use(&$odds){
            $odds *= $slip['odd_value'];
        });

		$totalOdd = round($odds,2);
		$this->session->set('betslip', $betslip);

		$count = sizeof($betslip);
		$this->view->setVars([
				'stake' => $stake, 
				'betslip'=>$betslip,
				'totalOdd' =>$totalOdd,
				'slipCount'=>$count]
			);
	}


	public function refreshAction(){

		$betslip = $this->session->get("betslip")?: [];
		$totalOdd = 1;
		$odds = 1;
        array_walk($betslip, function($slip, $mid)use(&$odds){
            $odds *= $slip['odd_value'];
        });

		$totalOdd = round($odds,2);
		$this->session->set('betslip', $betslip);

		$count = sizeof($betslip);

		$data = [
			'total'   => $count,
			'betslip' => $betslip
		];

		$response = new Response();
		$response->setStatusCode(201, "OK");
		$response->setHeader("Content-Type", "application/json");

		$response->setContent(json_encode($data));

		return $response;

	}

	public function stakeAction()
	{
		$stake = $this->request->get('stake') ?: 5;

		$this->session->set('stake', $stake);

		$this->flashSession->error($this->flashSuccess('Slip updated'));

		$this->response->redirect('betslip');
		// Disable the view to avoid rendering
		$this->view->disable();
	}

	public function addAction()
	{
		$match_id = $this->request->getPost('match_id', 'int');
		$home_team = $this->request->getPost('home');
		$away_team = $this->request->getPost('away');

		$status = 1;

		$betslip = [];
		$betslip_data = [];
		

        if ($this->session->has("betslip_data")) {
			$betslip_data = $this->session->get("betslip_data");
		}else{
			$betslip_data["total_odd"] = 1.00;
		}

		if ($this->session->has("betslip")) {
			$betslip = $this->session->get("betslip");
		}

		$checkOdd = $this->rawSelect("SELECT * from tip where game_id='$match_id' limit 1");
		$checkOdd = $checkOdd['0'];
		$prediction = $checkOdd['prediction'];
		$prediction_odds = 1;

		if($prediction == "HOME"){
			$prediction_odds = $checkOdd['home_odd'];
		}else if($prediction == "AWAY"){
			$prediction_odds = $checkOdd['away_odd'];
		}else if($prediction == "DRAW"){
			$prediction_odds = $checkOdd['draw_odd'];
		}else if($prediction == "1X"){
			$prediction_odds = $checkOdd['x1_odd'];
		}else if($prediction == "X2"){
			$prediction_odds = $checkOdd['x2_odd'];
		}else if($prediction == "12"){
			$prediction_odds = $checkOdd['one2_odd'];
		}else if($prediction == "OVER 2.5"){
			$prediction_odds = $checkOdd['over25_odd'];
		}else if($prediction == "UNDER 2.5"){
			$prediction_odds = $checkOdd['under25_odd'];
		}

		$betslip["$match_id"] = [
			'match_id'          => $match_id,
			'home_team'         => $home_team,
			'away_team'         => $away_team,
			'odd_value'         => $prediction_odds
		];

		$odds = 1;
        array_walk($betslip, function($slip, $mid)use(&$odds){
            $odds *= $slip['odd_value'];
        });

		$betslip_data['total_odd'] = round($odds, 2);

		$this->session->set("betslip", $betslip);
        $this->session->set("betslip_data", $betslip_data);

		$bets = $this->session->get('betslip');

		$count = sizeof($bets);
		$this->session->set("stake", $count);

		$data = [
			'status'  => $status,
			'total'   => $count,
			'betslip' => $betslip,
            'betslip_data' => $betslip_data,
		];

		$response = new Response();
		$response->setStatusCode(201, "OK");
		$response->setHeader("Content-Type", "application/json");

		$response->setContent(json_encode($data));

		return $response;
	}

	public function removeAction()
	{
		$match_id = $this->request->getPost('match_id', 'int');
		$bs = $this->request->get('bs', 'int');
		$betslip = $this->session->get("betslip");
		$betslip_data = $this->session->get("betslip_data");
       
		unset($betslip["$match_id"]);
        $odds = 1;
        array_walk($betslip, function($slip, $pmid)use(&$odds){
            $odds *= $slip['odd_value'];
        });

        $betslip_data['total_odd'] = round($odds, 2);

        $this->session->set("betslip_data", $betslip_data);
		$this->session->set("betslip", $betslip);
        
		if($bs !=1){
			$data = [
				'betslip' => $betslip,
				'betslip_data' => $betslip_data,
			];

			$response = new Response();
			$response->setStatusCode(201, "OK");
			$response->setHeader("Content-Type", "application/json");

			$response->setContent(json_encode($data));

			return $response;
        } else {
		    $this->flashSession->error($this->flashSuccess('Match successfully removed'));
	        $this->response->redirect('tipslip');
		    //Disable the view to avoid rendering
		    $this->view->disable();  
        }
	}

	public function clearslipAction()
	{

		$this->session->remove("betslip");

        $this->session->remove("betslip_data");
		$data = '1';

		$src = $this->request->getPost('src', 'string');
		//$this->flashSession->error($this->flashSuccess('Betslip cleared'));
		$this->response->redirect('/');
		// Disable the view to avoid rendering
		$this->view->disable();

	}

	public function checkoutAction()
	{
		$user_id = $this->request->getPost('user_id', 'int');
		$msisdn = $this->request->getPost('msisdn', 'int');
		$tip_amount = $this->request->getPost('stake', 'int');
		$total_odd = $this->request->getPost('total_odd', 'float');
		$src = $this->request->getPost('src', 'string') ?: 'internet';
		$betslip = $this->session->get('betslip');
		$endCustomerIP = $this->getClientIP();

		$response = new Response();
		$response->setStatusCode(201, "OK");
		$response->setHeader("Content-Type", "application/json");

		$msisdn = $this->formatMobileNumber($msisdn);

		if($tip_amount < 5){

			$this->flashSession->error($this->flashError('A minimum of 5 tips required. Please 
			add '.(5 - $tip_amount).' matches to get the tips'));
			$this->response->redirect('tipslip');
			$this->view->disable();
			return;
		}

		if (!($user_id || $msisdn) || !$tip_amount) {
			$this->flashSession->error($this->flashError('Please specify
			 your mobile phone number or login if you have registered!'));
			$this->response->redirect('tipslip');
			// Disable the view to avoid rendering
			$this->view->disable();
			return;
		} else {

			$checkUser = $this->rawSelect("SELECT * from `user` where phone_no='$msisdn' limit 1");

			if(!$checkUser){

				$password = $this->security->hash($msisdn);
				$profile_id = $this->rawInsert(
						"insert into user (phone_no,password,created_at) "
						. " values('$msisdn','$password',now())"
					);
				$profile_balance_id = $this->rawInsert(
						"insert ignore into profile_balance (user_id, balance,created_at)"
						. "values ( '$profile_id', 0.0, now()) "
					);
				
				$checkUser = $this->rawSelect("select * from `user` where phone_no='$msisdn' limit 1");
			}

			$loggedIn = $this->session->get('auth');

			$checkUser = $checkUser['0'];
			$mobile = $checkUser['phone_no'];
			$account = $checkUser['account'];
			$user_id = $checkUser['user_id'];

			$slip = [];
			$tip_message = "";

			$home_team = "";
			$away_team = "";
			$odd_value = 1.00;
			$game_id = 0;

			$feedback = "";

			foreach ($betslip as $match) {

				$home_team = $match['home_team'];
				$away_team = $match['away_team'];

				$tip_message = $tip_message."".$home_team." VERSUS ".$away_team."#";

			}

			$user_tip_id = $this->rawInsert(
				"insert into user_tip (user_tip_id,user_id,tip_message, total_odd,
				 tip_amount, endcustomer_ip, created_at, updated_at) 
				  values(null, '$user_id','$tip_message','$total_odd', '$tip_amount',
				   '$endCustomerIP', now(),now())"
			);

			$checkBalance = $this->rawSelect("SELECT * from `profile_balance` where
			 user_id='$user_id' limit 1");

			$checkBalance = $checkBalance['0'];
			$userbalance = $checkBalance['balance'];

			if($userbalance >= $tip_amount){

				$this->rawInsert(
					"update user_tip set status=2 where user_tip_id = '$user_tip_id'"
				);

				$smsMessage = "";

				foreach ($betslip as $match) {

					$game_id = $match['match_id'];
					$odd_value = $match['odd_value'];

					$checkTips = $this->rawSelect("SELECT * from `tip` where game_id='$game_id' limit 1");
					$checkTips = $checkTips['0'];
					$pred = $checkTips['prediction'];
					$home_team = $checkTips['home_team'];
					$away_team = $checkTips['away_team'];
					$start_time = $checkTips['start_time'];
					$subs = explode(" ", $start_time);
					$kick_off = substr($subs[1], 0, 5);
	
					$user_tipslip_id = $this->rawInsert(
						"insert into user_tipslip (user_tipslip_id,user_tip_id, game_id,home_team,
						away_team, odd_value, prediction, status, created_at, updated_at) 
						values(null,'$user_tip_id', '$game_id', '$home_team', '$away_team',
						'$odd_value', '$pred', 2, now(),now())"
					);

					$smsMessage = $smsMessage."ID ".$game_id." TIME: ".$kick_off." ".strtoupper(substr($home_team, 0, 6))." vs ".strtoupper(substr($away_team, 0, 6))." - ".strtoupper($pred)." ";

				}

				$this->rawInsert(
					"update profile_balance set balance = balance-$tip_amount where user_id = '$user_id'"
				);

				if($loggedIn && ($account == 1)){

					$feedback = "Your tips have been saved. Please go to \"My Tips\" page to view your tips.
					 Thank you.";
				}else if(!$loggedIn && ($account == 1)){

					$feedback = "Your tips have been saved. Please login and go to \"My Tips\" page to view your tips.
					 Thank you.";
				}else if(!$loggedIn && ($account == 0)){

					$feedback = "Your tips have been saved. Please login and go to \"My Tips\" page to view your tips. 
					Use your phone number as the password. Thank you.";
				}

				$smsMessage = $smsMessage." betika.tips TILL 731356";

				$this->rawInsert(
					"insert into outbox (id,msisdn,message,status,created_at, updated_at)
					  values(null, '$msisdn','$smsMessage','0',now(),now())"
				);

				$this->session->remove("betslip");
        		$this->session->remove("betslip_data");

			}else{

				foreach ($betslip as $match) {

					$home_team = $match['home_team'];
					$away_team = $match['away_team'];
					$game_id = $match['match_id'];
					$odd_value = $match['odd_value'];
	
					$user_tipslip_id = $this->rawInsert(
						"insert into user_tipslip (user_tipslip_id,user_tip_id, game_id,home_team,
						away_team, odd_value, prediction, created_at, updated_at) 
						values(null, '$user_tip_id', '$game_id', '$home_team', '$away_team',
						'$odd_value', null, now(),now())"
					);
				}

				// echo ApiController::paybillstkAction($tip_amount, $this->formatMobileNumber($msisdn));

				echo ApiController::stkAction($tip_amount, $this->formatMobileNumber($msisdn));

				if($loggedIn && ($account == 1)){

					$feedback = "Deposit prompt sent to your phone. Please pay and visit \"My Tips\"
					 page to view your tips. Thank you.";

				}else if(!$loggedIn && ($account == 1)){

					$feedback = "Deposit prompt sent to your phone. Please pay, login and go to
					 \"My Tips\" page to view your tips. Thank you.";

				}else if(!$loggedIn && ($account == 0)){

					$feedback = "Deposit prompt sent to your phone. Please pay, login and go to
					 \"My Tips\" page to view your tips. Use your phone number as the password. Thank you.";
				}

			}
		
			$this->flashSession->success($this->flashSuccess($feedback));

			$this->response->redirect('tipslip');

			$this->view->disable();
	}

}

private function httpPost($url, $data){
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}


private function array_msort($array, $cols)
{
	$colarr = [];
	foreach ($cols as $col => $order) {
		$colarr[$col] = [];
		foreach ($array as $k => $row) {
			$colarr[$col]['_' . $k] = strtolower($row[$col]);
		}
	}
	$eval = 'array_multisort(';
			foreach ($cols as $col => $order) {
			$eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
			}
			$eval = substr($eval, 0, -1) . ');';
	eval($eval);
	$ret = [];
	foreach ($colarr as $col => $arr) {
		foreach ($arr as $k => $v) {
			$k = substr($k, 1);
			if (!isset($ret[$k])) $ret[$k] = $array[$k];
			$ret[$k][$col] = $array[$k][$col];
		}
	}

	return $ret;
}

}

?>
