<?php

class MatchController extends ControllerBase
{
    public function IndexAction()
    {
        $id = $this->request->get('id', 'int');
        $cache_key = 'match'.$id;

        list($matchInfo, $subTypes) = null; //$this->redisCache->get($cache_key);

		if(empty($matchInfo)){

			$matchInfo = $this->rawSelect(
				"select s.sport_id, m.home_team, m.game_id, "
				 . " m.away_team,m.start_time,c.competition_name,c.category,"
				 . " m.parent_match_id from `match` m left join competition c "
				 . " on m.competition_id=c.competition_id "
				 . " inner join sport s on c.sport_id = s.sport_id "
				 . " where match_id='$id' and m.start_time > now() limit 1");

        	$matchInfo = array_shift($matchInfo);
			$subTypes = $this->rawSelect(
				"select o.priority, m.match_id, e.odd_key as display, o.name, "
				. " e.odd_key, e.odd_value, e.sub_type_id, e.special_bet_value "
				. " from event_odd e inner join odd_type o on (e.parent_match_id "
				. " = o.parent_match_id and e.sub_type_id = o.sub_type_id and "
				. " e.special_bet_value = o.special_bet_value) inner join `match` "
				. " m on m.parent_match_id = e.parent_match_id where m.start_time  > now() "
				. " and o.sub_type_id not in (65,66,16, 882) and match_id = '$id' "
				. " and o.live_bet = 0 and o.active = 1 and e.odd_key <> '-1' "
				. " and CASE WHEN e.sub_type_id = 18 THEN e.odd_key REGEXP '[.]5$' "
				. " WHEN e.sub_type_id = 68 THEN e.odd_key REGEXP '[.]5$' "
				. " WHEN e.sub_type_id = 90 THEN e.odd_key REGEXP '[.]5$' ELSE  1=1 END "
				. " and case when e.sub_type_id =1 then e.odd_key in "
				. " (m.home_team, 'draw', m.away_team) else 1=1 end group by "
				. " e.sub_type_id, e.odd_key, o.name order by "
				. " field(o.name, 'Which team to score', 'Goal Range', 'Correct score',  '1st half - correct score',"
				. " 'Halftime/fulltime','Draw No bet', '1st goal', '1st half - 1x2',"
				. " 'Total', 'Both Teams to Score', "
				. " 'Double Chance', '1X2') desc, "
				. " sub_type_id,"
			#	. " field(e.odd_key, m.home_team,  'draw', m.away_team, 'over', 'under') desc, "
				. " o.special_bet_value asc"
			);
            $max_cache_time = time()+600;
            $match_time = strtotime($matchInfo['start_time']);
            $lifetime = ($match_time > $max_cache_time ? $max_cache_time : $match_time) - time();

            $this->redisCache->set($cache_key, [$matchInfo, $subTypes], $lifetime);
		}

		$sport_id = $matchInfo['sport_id'];

		$current_sport = [];
		foreach($this->sports as $key => $sp){
			if($sp['sport_id'] == $sport_id){
				$current_sport = $sp;
				break;
			}
		}

        
        $theBetslip = $this->session->get("betslip");
        $title = $matchInfo['home_team'] . " vs " . $matchInfo['away_team'];

        $this->tag->setTitle($title);

        $this->tag->setTitle("Title");

       $this->view->setVars([
            'subTypes'   => $subTypes,
            'matchInfo'  => $matchInfo,
            'theBetslip' => $theBetslip,
			'current_sport' => $current_sport,
        ]);


    }

}

?>
