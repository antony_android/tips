<?php

/**
 * Copyright (c) Anto 2018.
 *
 * All rights reserved.
 */

use Phalcon\Http\Response;

class LiveController extends ControllerBase
{

    /**
     * Results controller to get the results, outcome hardcoded to be changed later 
     */
    public function indexAction()
    {

        $hour = date('H');
        $sport_id = $this->request->get('sp', 'int');

        list($page, $limit, $skip) = $this->getPaginationParams();

        $keyword = $this->request->getPost('keyword', 'string');

       
        $where = "";
        if ($sport_id){
            $where = " and s.sport_id = '$sport_id'";
        }

        $orderBy = "m.priority desc, c.priority desc, m.start_time asc";

        list($today, $total, $sCompetitions) = $this->getLiveGames($keyword, $skip, $limit, $where, $orderBy);

        $total = $total['0']['total'];

        $theBetslip = $this->session->get("betslip");
        $newslip = [];
        $totalOdd = 1;
        foreach($theBetslip as $match_id => $slip){
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_type =  $slip['bet_type'];
            $query = "select e.odd_value, e.odd_active, e.market_active, e.active as eactive, lo.active as m_active from "
                . " live_odds_change e inner join live_match lo using(parent_match_id) where "
                . " e.parent_match_id='$parent_match_id' and e.sub_type_id='$sub_type_id' "
                . " and e.special_bet_value='$special_bet_value' and e.odd_key='$bet_pick' "
                . " and lo.active = 1 and lo.event_status = 'Live' order by e.betradar_timestamp desc limit 1 ";

        
            $data = $this->rawQueries($query);
            if(!empty($data) && 
                $data[0]['odd_active'] == 1 && $data[0]['market_active'] == 'Active' 
                && $data[0]['eactive'] == 1 && $data[0]['m_active'] == 1){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;
            }else{
                $slip['odd_value'] = 1;
                $newslip[$match_id] = $slip; 
            }
            $totalOdd *=  $slip['odd_value'];
        }
        //die(print_r($newslip, 1));
        $theBetslip = $newslip;
        $this->session->set('betslip', $theBetslip);

        $this->view->setVars([
            'today'         => $today,
            'theBetslip'    => $theBetslip,
            'sCompetitions' => $sCompetitions,
            'total'         => $total,
            'pages'         => $this->getResultPages($total, $limit),
            'page'          => $page,
            'topSports'     => $this->topSports(),
            'sportId' =>$sport_id,
            'keyword' =>$keyword,
            'totalOdd'=>$totalOdd
        ]);

        $this->tag->setTitle($this->view->t->_('title'));
        $this->view->pick("live/index");

    }


    public function fetchgamesAction()
    {

        $max_delay_time = strtotime("+20 seconds");

        $last_upadated =  $this->session->get("l_last_update_datetime");
        
        while(!empty($last_upadated)){
            $updateQuery = "select c.modified from live_odds_change c inner join live_match using(parent_match_id) where c.sub_type_id =1 and  c.modified > '$last_upadated' order by 1 desc limit 1";
            $lastUpdateDateResult = $this->rawQueries($updateQuery);

            if(!empty($lastUpdateDateResult)){
                $lastUpdateDateResult = $lastUpdateDateResult[0]['modified'];
                $this->session->set('l_last_update_datetime', $lastUpdateDateResult);
                break;
            }
            if(time() > $max_delay_time){
                //Alow update on UI after max delay time
                break;
            }
            usleep(3000);
            
        }
        if(empty($last_upadated)){
            $updateQuery = "select now() as modified ";
            $lastUpdateDateResult = $this->rawQueries($updateQuery);
            if(!empty($lastUpdateDateResult)){
                $lastUpdateDateResult = $lastUpdateDateResult[0]['modified'];
                $this->session->set('l_last_update_datetime', $lastUpdateDateResult);
            }

        }
        // proceed
        $theBetslip = $this->session->get("betslip_live");

        $keyword = $this->request->getPost('keyword', 'string');

        $where = "";

        $bindings = null;
        $orderBy = "m.priority desc, c.priority desc, m.start_time asc";
       
        
        $query = "SELECT c.priority, '' AS side_bets, o.sub_type_id, 
            group_concat(if(o.odd_key = m.home_team, o.odd_value, null)) as home_odd, 
            group_concat(if(o.odd_key = 'draw', o.odd_value, null)) as neutral_odd, 
            group_concat(if(o.odd_key = m.away_team, o.odd_value, null)) as away_odd,  
            group_concat(if(o.odd_key = m.home_team, o.active, null)) as home_odd_status, 
            group_concat(if(o.odd_key = 'draw', o.active, null)) as draw_odd_status, 
            group_concat(if(o.odd_key = m.away_team, o.active, null)) as away_odd_status, 
            o.market_active, m.match_id, m.start_time, m.away_team, m.home_team,
            m.parent_match_id, c.competition_name,c.category, m.match_time, 
            m.score, m.bet_status, m.match_status, m.event_status, m.home_red_card, 
            m.away_red_card, m.home_yellow_card, m.away_yellow_card, m.home_corner, 
            m.away_corner, m.active as event_live FROM `live_odds_change` o inner join
            live_match m on o.parent_match_id=m.parent_match_id INNER JOIN competition c 
            ON c.competition_id = m.competition_id INNER JOIN sport s 
            ON s.sport_id = c.sport_id where s.sport_id=79 AND 
            o.sub_type_id =1 and m.event_status='Live' and m.active=1 GROUP BY m.parent_match_id,
            o.sub_type_id,o.special_bet_value ORDER BY $orderBy LIMIT 0,200";
         
        $games = $this->rawQueries($query, $bindings);

        $theBetslip = $this->session->get("betslip");
        $newslip = [];
        $totalOdd = 1;
        foreach($theBetslip as $match_id => $slip){
            $sub_type_id = $slip['sub_type_id'];
            $parent_match_id = $slip['parent_match_id'];
            $special_bet_value = $slip['special_bet_value'];
            $bet_pick = $slip['bet_pick'];
            $bet_type =  $slip['bet_type'];
            $query = "select e.odd_value, e.odd_active, e.market_active from "
                . " live_odds_change e inner join live_match lo using(parent_match_id) where "
                . " e.parent_match_id='$parent_match_id' and e.sub_type_id='$sub_type_id' "
                . " and e.special_bet_value='$special_bet_value' and e.odd_key='$bet_pick' "
                . " and e.active = 1 and lo.event_status = 'Live' order by e.betradar_timestamp desc limit 1 ";

        
            $data = $this->rawQueries($query);
            if(!empty($data) && 
                $data[0]['odd_active'] == 1 && $data[0]['market_active'] == 'Active'  ){
                $new_odd = $data[0]['odd_value'];
                $slip['odd_value'] = $new_odd;
                $newslip[$match_id] = $slip;
            }else{
                $slip['odd_value'] = 1;
                $newslip[$match_id] = $slip; 
            }
            $totalOdd *=  $slip['odd_value'];
        }
        //die(print_r($newslip, 1));
        $theBetslip = $newslip;
        $this->session->set('betslip', $theBetslip);

        $results = [
            'games'   => $games,
            'theBetslip' => $theBetslip,
            'totalOdd'=>$totalOdd
        ];

        $response = new Response();
        $response->setStatusCode(201, "OK");
        $response->setHeader("Content-Type", "application/json");

        $response->setContent(json_encode($results));

        return $response;
    }

}
