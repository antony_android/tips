function refreshSlip() {
    $.post("tipslip", {}, function (data) {
        $("#betslip").animate({opacity: '0.8'});
        $("#betslip").html(data);
        $("#betslip").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function refreshMatchesSelection() {

    $.post("tipslip/refresh", {}, function (data) {
        
        $(".tippy").removeClass('picked');
        var tipslip = data.betslip;
        $.each(tipslip, function( index, value ) {
            console.log("Updating "+index+" component.");
            $("#"+index).addClass('picked');
        });

    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function refreshJackSlip() {
    $.post("matches/jackpot", {}, function (data) {
        $("#betslipJ").animate({opacity: '0.8'});
        $("#betslipJ").html(data);
        $("#betslipJ").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}


function refreshBingwaFour() {
    $.post("tipslip/bingwa", {}, function (data) {
        $("#betslipB").animate({opacity: '0.8'});
        $("#betslipB").html(data);
        $("#betslipB").animate({opacity: '1'});
    }).done(function () {
        $(".loader").css("display", "none");
    });
}

function addTip(value, home, away) {
    var self = this;
    if ($('.tip-' + value).hasClass('picked')) {
        var counterHolder = $(".slip-counter"),
            count = counterHolder.html() * 1;
        counterHolder.html(--count);
        return removeTip(value);
    }
    $(".loader").slideDown("slow");
    $.post("tipslip/add", {
        match_id: value,
        home: home,
        away: away
    }, function (data) {
        $(".tip-" + value).removeClass('picked');
        $(self).addClass('picked');
        $(".tip-" + value).addClass('picked');
        //add odds and recalculate values
        
        var bet_slip_count_label = document.getElementById("betslip-count");
        var stake_field = document.getElementById("stake");
        var slip_count = Object.keys(data.betslip).length;
        bet_slip_count_label.innerHTML = slip_count;
        stake_field.value = slip_count;
        
    });
}

function removeTip(value) {
    $(".loader").slideDown("slow");
    $.post("tipslip/remove", {match_id: value}, function (data) {
        //refreshSlip();
        $(".tip-" + value).removeClass('picked');
        console.log(data);
        
        var bet_slip_count_label = document.getElementById("betslip-count");
        var stake_field = document.getElementById("stake");
        var slip_count = Object.keys(data.betslip).length;
        bet_slip_count_label.innerHTML = slip_count;
        stake_field.value = slip_count;

    });
}

function clearSlip(value) {
    $(".loader").slideDown("slow");
    $.post("tipslip/clearslip", {}, function (data) {
        refreshSlip();
        refreshMatchesSelection();
        $(".picked").removeClass('picked');
    });
}

function winnings() {
    var value = $("#bet_amount").val();
    var odds = $("#total_odd").val();
    var totalWin = value * odds;
    var totalWin = Math.round(totalWin);
    $("#pos_win").html(totalWin);
}


function winningsM() {
    var value = $("#bet_amount_m").val();
    var odds = $("#total_odd_m").val();
    var totalWin = value * odds;
    var totalWin = Math.round(totalWin);
    $("#pos_win_m").html(totalWin);
}

function showMpesaForm(){
  var tg = document.getElementById("tigopesa");
  tg.style.display='none';
  var am = document.getElementById("airtelmoney")
  am.style.display='none';
  var mp = document.getElementById("mpesa");
  mp.style.display='block';
  return false;
}

function showTigoForm(){
  var mp = document.getElementById("mpesa");
  mp.style.display='none';
  var am = document.getElementById("airtelmoney")
  am.style.display='none';
  var tg = document.getElementById("tigopesa");
  tg.style.display='block';
  return false;
}
function showAirtelForm(){
 var mp = document.getElementById("mpesa");
  mp.style.display='none';
  var tg = document.getElementById("tigopesa");
  tg.style.display='none';
  var am = document.getElementById("airtelmoney")
  am.style.display='block';
  return false;
}

refreshMatchesSelection();